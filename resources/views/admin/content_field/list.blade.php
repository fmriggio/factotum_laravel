@extends('admin.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-info">
                    <div class="panel-heading">Lista Campi di Contenuto</div>

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Content Type</th>
                            <th>Add Field</th>
                            <th>Delete Field</th>
                        </tr>

                        @foreach ($contentTypes as $contentType)
                            <tr>
                                <th scope="row">{{ $contentType->id }}</th>
                                <td>
                                    <a href="{{ url('/admin/content-field/detail/' . $contentType->id) }}">{{ $contentType->content_type }}</a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/content-field/create/' . $contentType->id) }}"
                                       class="btn btn-default btn-info">Add New Field</a>
                                </td>
                                <td></td>
                            </tr>

                            @if ($contentType->content_fields)
                                @foreach ($contentType->content_fields as $field)
                                <tr>
                                    <td>{{ $field->id }}</td>
                                    <td>{{ $field->label }}</td>
                                    <td>
                                        <a href="{{ url('/admin/content-field/edit/' . $contentType->id . '/' . $field->id ) }}">Edit</a>
                                    </td>
                                    <td>
                                        <a href="{{ url('/admin/content-field/delete/' . $field->id ) }}">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            @endif

                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

<?php

$imageSectionClass = 'hidden';

if ( isset($contentField) &&
		($contentField->type == 'image_upload' ||
				$contentField->type == 'gallery') ) {
	$imageSectionClass = 'visible';
}

if (old('type') == 'image_upload' || old('type') == 'gallery' ) {
	$imageSectionClass = 'visible';
}

?>

<div id="image_section" class="{{ $imageSectionClass }}">

	<!-- Image Max Width Size -->
	<div class="form-group{{ $errors->has('min_width_size') ? ' has-error' : '' }}">
		<label for="min_width_size" class="col-md-4 control-label">Min Width Size</label>

		<div class="col-md-6">
			<input id="min_width_size" type="text" class="form-control"
				   name="min_width_size" value="{{ old('min_width_size', (isset($contentField) ? $contentField->min_width_size : null)) }}" autofocus>

			@if ($errors->has('min_width_size'))
				<span class="help-block">
					<strong>{{ $errors->first('min_width_size') }}</strong>
				</span>
			@endif
		</div>
	</div>

	<!-- Image Max Height Size -->
	<div class="form-group{{ $errors->has('min_height_size') ? ' has-error' : '' }}">
		<label for="max_height_size" class="col-md-4 control-label">Min Height Size</label>

		<div class="col-md-6">
			<input id="min_height_size" type="text" class="form-control"
				   name="min_height_size" value="{{ old('min_height_size', (isset($contentField) ? $contentField->min_height_size : null)) }}" autofocus>

			@if ($errors->has('min_height_size'))
				<span class="help-block">
					<strong>{{ $errors->first('min_height_size') }}</strong>
				</span>
			@endif
		</div>
	</div>

	<!-- Image Operation -->
	<div class="form-group{{ $errors->has('image_operation') ? ' has-error' : '' }}">
		<label for="field_image_operation" class="col-md-4 control-label">Image Operation</label>

		<div class="col-md-6">
			<select name="image_operation" id="field_image_operation" class="form-control" required autofocus>
				@foreach ($imageOperations as $imageOp => $imageOpLabel)
					<option value="{{ $imageOp }}"
					<?php echo ( old('image_operation', (isset($contentField) ? $contentField->image_operation : null)) == $imageOp ? 'selected' : ''); ?>>{{ $imageOpLabel }}</option>
				@endforeach
			</select>

			@if ($errors->has('image_operation'))
				<span class="help-block">
					<strong>{{ $errors->first('image_operation') }}</strong>
				</span>
			@endif
		</div>
	</div>

	<!-- Image B/W -->
	<div class="form-group{{ $errors->has('image_bw') ? ' has-error' : '' }}">
		<label for="image_bw" class="col-md-4 control-label">Image Black & White</label>
		<div class="col-md-6">
			<div class="form-control" style="border: none; box-shadow: none;">
				<input type="checkbox" id="image_bw"
					   name="image_bw" value="1"
				<?php echo (isset($contentField) && $contentField->image_bw ? ' checked' : ''); ?>>
			</div>
		</div>
	</div>

	<div id="resizes_section">
		<span style="color: red;">add drag and drop for sort the options</span><br>
		<button class="btn btn-primary" id="add_resize">Add Resize</button>
		<br><br>
		<div id="resize_list">

<?php
if ( isset($contentField) && count($contentField->resizes) > 0 ) {

	$index = 0;
	foreach ( $contentField->resizes as $width => $height ) {
?>
			<div class="form-group row clearfix resize" data-no="{{ $index }}">
				<div class="col-xs-5">
					<input type="text" class="form-control"
						   placeholder="Width Size"
						   name="width_resize[]" value="{{ $width }}" autofocus>
				</div>
				<div class="col-xs-5">
					<input type="text" class="form-control"
						   placeholder="Height Size"
						   name="height_resize[]" value="{{ $height }}" autofocus>
				</div>

				@if ($index > 0)
					<div class=col-xs-2"">
						<button class="remove_resize btn btn-danger">remove</button>
					</div>
				@endif

			</div>

<?php
		$index++;
	}
}
?>

		</div>
	</div>

</div>


<?php

$optionsSectionClass = 'hidden';

if ( isset($contentField) &&
	 ( $contentField->type == 'select' || $contentField->type == 'multiselect' ||
	   $contentField->type == 'checkbox' || $contentField->type == 'multicheckbox' ||
	   $contentField->type == 'radio' ) ) {
	$optionsSectionClass = 'visible';
}

if ( old('type') == 'select' || old('type') == 'multiselect' ||
	 old('type') == 'checkbox' || old('type') == 'multicheckbox' ||
	 old('type') == 'radio') {
	$optionsSectionClass = 'visible';
}

?>

<div id="options_section" class="<?php echo $optionsSectionClass; ?>">
	<span style="color: red;">add drag and drop for sort the options</span><br>
	<button class="btn btn-primary" id="add_option">Add Option</button>
	<br><br>
	<div id="options_list">

<?php
if ( isset($contentField) && count($contentField->options) > 0 ) {

	$index = 0;
	foreach ( $contentField->options as $value => $label) {
?>
			<div class="form-group row clearfix option" data-no="{{ $index }}">
				<div class="col-xs-5">
					<input type="text" class="form-control"
						   placeholder="Option Value"
						   name="option_value[]" value="{{ $value }}" autofocus>
				</div>
				<div class="col-xs-5">
					<input type="text" class="form-control"
						   placeholder="Option Label"
						   name="option_label[]" value="{{ $label }}" autofocus>
				</div>

				<?php if ($index > 0) { ?>
				<div class=col-xs-2"">
					<button class="remove_option btn btn-danger">remove</button>
				</div>
				<?php } ?>

			</div>

<?php
		$index++;
	}

}
?>

	</div>
</div>

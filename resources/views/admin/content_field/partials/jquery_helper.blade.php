<div class="hidden">
	<div class="form-group row clearfix option" id="single_resize_template">
		<div class="col-xs-5">
			<input type="text" class="form-control"
				   placeholder="Width Resize"
				   name="width_resize[]"  autofocus>
		</div>
		<div class="col-xs-5">
			<input type="text" class="form-control"
				   placeholder="Height Resize"
				   name="height_resize[]" autofocus>
		</div>
		<div class=col-xs-2"">
			<button class="remove_resize btn btn-danger">remove</button>
		</div>
	</div>
</div>

<div class="hidden">
	<div class="form-group row clearfix option" id="single_option_template">
		<div class="col-xs-5">
			<input type="text" class="form-control"
				   placeholder="Option Value"
				   name="option_value[]"  autofocus>
		</div>
		<div class="col-xs-5">
			<input type="text" class="form-control"
				   placeholder="Option Label"
				   name="option_label[]" autofocus>
		</div>
		<div class=col-xs-2"">
			<button class="remove_option btn btn-danger">remove</button>
		</div>
	</div>
</div>
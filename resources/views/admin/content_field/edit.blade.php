@extends('admin.layouts.app')

@section('content')

	<?php var_dump($errors->toArray()); ?>

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">

					<div class="panel-heading">{{ $title }}</div>

					<div class="panel-body">
						<form class="form-horizontal" role="form" method="POST" action="{{ $postUrl }}">
							{{ csrf_field() }}

							<div class="form-group{{ $errors->has('label') ? ' has-error' : '' }}">
								<label for="field_label" class="col-md-4 control-label">Field Label</label>

								<div class="col-md-6">
									<input id="field_label" type="text" class="form-control"
										   name="label" value="{{ old('label', (isset($contentField) ? $contentField->label : null)) }}" required autofocus>

									@if ($errors->has('label'))
										<span class="help-block">
										<strong>{{ $errors->first('label') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label class="col-md-4 control-label">Field Name</label>

								<div class="col-md-6">
									<input id="field_name" type="text" class="form-control" readonly
										  name="name" value="{{ old('name', (isset($contentField) ? $contentField->name : null)) }}" required autofocus>

									@if ($errors->has('name'))
										<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<input type="hidden"
								   id="field_name_hidden"
								   name="name" value="{{ old('name', (isset($contentField) ? $contentField->name : null)) }}">


							<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
								<label for="field_type" class="col-md-4 control-label">Field Type</label>

								<div class="col-md-6">
									<select name="type" id="field_type" class="form-control" required autofocus>
										@foreach ($fieldTypes as $fieldTypeInd => $fieldType)
											<option value="{{ $fieldTypeInd }}"
											<?php echo ( old('type', (isset($contentField) ? $contentField->type : null)) == $fieldTypeInd ? 'selected' : ''); ?>>{{ $fieldType }}</option>
										@endforeach
									</select>

									@if ($errors->has('type'))
										<span class="help-block">
										<strong>{{ $errors->first('type') }}</strong>
									</span>
									@endif
								</div>
							</div>


							<div class="form-group{{ $errors->has('mandatory') ? ' has-error' : '' }}">
								<label for="mandatory" class="col-md-4 control-label">Mandatory</label>
								<div class="col-md-6">
									<div class="form-control" style="border: none; box-shadow: none;">
										<input type="checkbox" id="mandatory"
											   name="mandatory" value="1"
										<?php echo (isset($contentField) && $contentField->mandatory ? ' checked' : ''); ?>>
									</div>
								</div>
							</div>


							<div class="form-group{{ $errors->has('hint') ? ' has-error' : '' }}">
								<label for="hint" class="col-md-4 control-label">Field Hint</label>

								<div class="col-md-6">
									<input id="hint" type="text" class="form-control"
										   name="hint" value="{{ old('hint', (isset($contentField) ? $contentField->hint : null)) }}" autofocus>
								</div>
							</div>

@include('admin.content_field.partials.options')

@include('admin.content_field.partials.files')

@include('admin.content_field.partials.images')

@include('admin.content_field.partials.linked_content')


							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@include('admin.content_field.partials.jquery_helper')

@endsection

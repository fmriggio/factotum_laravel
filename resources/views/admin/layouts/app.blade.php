<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
	<link href="/factotum/css/main.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Factotum') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/admin/auth/login') }}">Login</a></li>
                        <li><a href="{{ url('/admin/user/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->profile->first_name }}
                                {{ Auth::user()->profile->last_name }}
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/admin/user/list') }}">User List</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/role/list') }}">Role List</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/capability/list') }}">Capability List</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/content-type/list') }}">Content Type List</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/content-field/list') }}">Content Field List</a>
                                </li>

                                @foreach ($contentTypes as $contentType)
                                <li>
                                    <a href="{{ url('/admin/content/list/' . $contentType->id) }}">{{ $contentType->content_type }}</a>
                                </li>
                                @endforeach

                                <li>
                                    <a href="{{ url('/admin/auth/logout') }}">Logout</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @yield('content')



<!-- Scripts -->
<script src="/factotum/js/main.js"></script>

</body>
</html>
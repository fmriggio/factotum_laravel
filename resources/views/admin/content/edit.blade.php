@extends('admin.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">{{ $title }}</div>

                    <div class="panel-body">

<?php var_dump($errors->toArray()); ?>

                        <form class="form-horizontal" role="form" method="POST" action="{{ $postUrl }}" enctype="multipart/form-data">

                            {{ csrf_field() }}

                            <!--
                            content_type_id
                            user_id

                            status
                            parent_content_id
                            lang
                            -->

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="field_label" class="col-md-2 control-label">Title</label>

                                <div class="col-md-10">
                                    <input id="title" type="text" class="form-control"
                                           name="title" value="{{ old('title', (isset($content) ? $content->title : null)) }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-2 control-label">URL</label>

                                <div class="col-md-10">
                                    <input id="url" type="text" class="form-control"
                                          name="url" value="{{ old('url', (isset($content) ? $content->url : null)) }}" required autofocus>

                                    @if ($errors->has('url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


							<div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
								<label for="parent_id" class="col-md-2 control-label">Parent Content</label>

								<div class="col-md-10">
									<?php PrintDropdownTree::print_dropdown( $contentsTree, 'parent_id', old('parent_id', (isset($content) ? $content->parent_id : null)), 'parent_id', 'form-control' ); ?>

									@if ($errors->has('parent_id'))
										<span class="help-block">
											<strong>{{ $errors->first('parent_id') }}</strong>
										</span>
									@endif
								</div>
							</div>


                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-2 control-label">Status</label>

                                <div class="col-md-10">
                                    <select name="status" id="status" class="form-control" required autofocus>
                                        @foreach ($statuses as $status => $label)
                                            <option value="{{ $status }}"
                                            <?php echo ( old('status', (isset($content) ? $content->status : null)) == $status ? 'selected' : ''); ?>>{{ $label }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <!-- Main Content -->
                            <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                                <label for="content" class="col-md-2 control-label">Content</label>

                                <div class="col-md-10">
                                    <textarea name="content" id="content" class="form-control wysiwyg"
                                         rows="5">{{ old('content', (isset($content) ? $content->content : null)) }}</textarea>

                                    @if ($errors->has('content'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

<?php

if ( count($contentFields) > 0 ) {

    foreach ($contentFields as $field) {
		PrintField::print_field( $field, $errors, (isset($additionalValues) ? $additionalValues->{$field->name} : null) );
    }

}

?>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('admin.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="btn-group" role="group">
                    <a href="{{ url('/admin/content/create/' . $contentTypeId) }}" class="btn btn-default btn-info">Add New</a>
                </div>

                <br><br>

                <div class="panel panel-info">
                    <div class="panel-heading">Lista Contenuti</div>

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>

						<?php PrintContentsTree::print_list($contents); ?>

                    </table>

					<div style="text-align: center;">
						{{ $contents->links() }}
					</div>
                </div>
            </div>
        </div>
    </div>

@endsection
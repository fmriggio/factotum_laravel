@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">{{ $title }}</div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ $postUrl }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                                <label for="role" class="col-md-4 control-label">Role</label>

                                <div class="col-md-6">
                                    <input id="role" type="text" class="form-control"
                                           name="role" value="{{ old('role', (isset($role) ? $role->role : null)) }}" required autofocus>

                                    @if ($errors->has('role'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('backend_access') ? ' has-error' : '' }}">
                                <label for="backend_access" class="col-md-4 control-label">Access Backend</label>
                                <div class="col-md-6">
                                    <div class="form-control" style="border: none; box-shadow: none;">
                                        <input type="checkbox" id="backend_access"
                                               name="backend_access" value="1"
                                               <?php echo (isset($role) && $role->backend_access ? ' checked' : ''); ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('manage_content_types') ? ' has-error' : '' }}">
                                <label for="manage_content_types" class="col-md-4 control-label">Manage Content Types</label>
                                <div class="col-md-6">
                                    <div class="form-control" style="border: none; box-shadow: none;">
                                        <input type="checkbox" id="manage_content_types"
                                               name="manage_content_types" value="1"
                                               <?php echo (isset($role) && $role->manage_content_types ? ' checked' : ''); ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('manage_users') ? ' has-error' : '' }}">
                                <label for="manage_users" class="col-md-4 control-label">Manage Users</label>
                                <div class="col-md-6">
                                    <div class="form-control" style="border: none; box-shadow: none;">
                                        <input type="checkbox" id="manage_users"
                                               name="manage_users" value="1"
                                               <?php echo (isset($role) && $role->manage_users ? ' checked' : ''); ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('manage_content_categories') ? ' has-error' : '' }}">
                                <label for="manage_content_categories" class="col-md-4 control-label">Manage Categories</label>
                                <div class="col-md-6">
                                    <div class="form-control" style="border: none; box-shadow: none;">
                                        <input type="checkbox" id="manage_content_categories"
                                               name="manage_content_categories" value="1"
                                               <?php echo (isset($role) && $role->manage_content_categories ? ' checked' : ''); ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

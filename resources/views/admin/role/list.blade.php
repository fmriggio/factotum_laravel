@extends('admin.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="btn-group" role="group">
                    <a href="{{ url('/admin/role/create') }}" class="btn btn-default btn-info">Add New</a>
                </div>

                <br><br>

                <div class="panel panel-info">
                    <div class="panel-heading">Lista Ruoli</div>

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Role</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>

                        @foreach ($roles as $role)
                            <tr>
                                <th scope="row">{{ $role->id }}</th>
                                <td>
                                    <a href="{{ url('/admin/role/detail/' . $role->id) }}">{{ $role->role }}</a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/role/edit/' . $role->id) }}">Edit</a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/role/delete/' . $role->id) }}">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('admin.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="btn-group" role="group">
                    <a href="{{ url('/admin/user/create') }}" class="btn btn-default btn-info">Add New</a>
                </div>

                <br><br>

                <div class="panel panel-info">
                    <div class="panel-heading">Lista Utenti</div>

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>

                        @foreach ($users as $user)
                            <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td>{{ $user->profile->first_name }}</td>
                                <td>{{ $user->profile->last_name }}</td>
                                <td>
                                    <a href="{{ url('/admin/user/detail/' . $user->id) }}">{{ $user->email }}</a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/user/edit/' . $user->id) }}">Edit</a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/user/delete/' . $user->id) }}">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">{{ $title }}</div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ $postUrl }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('content_type') ? ' has-error' : '' }}">
                                <label for="content_type" class="col-md-4 control-label">Content Type</label>

                                <div class="col-md-6">
                                    <input id="content_type" type="text" class="form-control"
                                           name="content_type" value="{{ old('role', (isset($contentType) ? $contentType->content_type : null)) }}" required autofocus>

                                    @if ($errors->has('content_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('content_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

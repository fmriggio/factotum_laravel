@extends('admin.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="btn-group" role="group">
                    <a href="{{ url('/admin/content-type/create') }}" class="btn btn-default btn-info">Add New</a>
                </div>

                <br><br>

                <div class="panel panel-info">
                    <div class="panel-heading">Lista Tipi di Contenuto</div>

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Content Type</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>

                        @foreach ($contentTypes as $contentType)
                            <tr>
                                <th scope="row">{{ $contentType->id }}</th>
                                <td>
                                    <a href="{{ url('/admin/content-type/detail/' . $contentType->id) }}">{{ $contentType->content_type }}</a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/content-type/edit/' . $contentType->id) }}">Edit</a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/content-type/delete/' . $contentType->id) }}">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('admin.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="btn-group" role="group">
                    <a href="{{ url('/admin/capability/create') }}" class="btn btn-default btn-info">Add New</a>
                </div>

                <br><br>

                <div class="panel panel-info">
                    <div class="panel-heading">Lista Capabilities</div>

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Role</th>
                            <th>Content Type</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>

                        @foreach ($capabilities as $capability)
                            <tr>
                                <th scope="row">{{ $capability->id }}</th>
                                <td>
                                    <a href="{{ url('/admin/capability/detail/' . $capability->id) }}">
                                        {{ $capability->role->role }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/capability/detail/' . $capability->id) }}">
                                        {{ $capability->content_type->content_type }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/capability/edit/' . $capability->id) }}">Edit</a>
                                </td>
                                <td>
                                    <a href="{{ url('/admin/capability/delete/' . $capability->id) }}">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">{{ $title }}</div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ $postUrl }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Role</label>

                                <div class="col-md-6">
                                    <select name="role_id" class="form-control" required autofocus>
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}"
                                            <?php echo ( old('role_id', (isset($user) ? $user->role_id : null)) == $role->id ? 'selected' : ''); ?>>{{ $role->role }}</option>
                                        @endforeach
                                    </select>


                                    @if ($errors->has('role_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('role_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('content_type_id') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Content Type</label>

                                <div class="col-md-6">
                                    <select name="content_type_id" class="form-control" required autofocus>
                                        @foreach ($contentTypes as $contentType)
                                            <option value="{{ $contentType->id }}"
                                            <?php echo ( old('content_type_id', (isset($capability) ? $capability->content_type_id : null)) == $contentType->id ? 'selected' : ''); ?>>{{ $contentType->content_type }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('content_type_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('content_type_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('configure') ? ' has-error' : '' }}">
                                <label for="configure" class="col-md-4 control-label">Configure</label>
                                <div class="col-md-6">
                                    <div class="form-control" style="border: none; box-shadow: none;">
                                        <input type="checkbox" id="configure"
                                               name="configure" value="1"
                                               <?php echo (isset($capability) && $capability->configure ? ' checked' : ''); ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('edit') ? ' has-error' : '' }}">
                                <label for="edit" class="col-md-4 control-label">Edit</label>
                                <div class="col-md-6">
                                    <div class="form-control" style="border: none; box-shadow: none;">
                                        <input type="checkbox" id="edit"
                                               name="edit" value="1"
                                               <?php echo (isset($capability) && $capability->edit ? ' checked' : ''); ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('publish') ? ' has-error' : '' }}">
                                <label for="publish" class="col-md-4 control-label">Publish</label>
                                <div class="col-md-6">
                                    <div class="form-control" style="border: none; box-shadow: none;">
                                        <input type="checkbox" id="publish"
                                               name="publish" value="1"
                                               <?php echo (isset($capability) && $capability->publish ? ' checked' : ''); ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

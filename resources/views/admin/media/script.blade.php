<script>
var ds            = '/';
var home_dir      = ds;
var shared_folder = ds + "{{ Config::get('factotum.media_folder_name') }}";
var image_url     = "{{ asset(Config::get('factotum.images_url')) }}";
var file_url      = "{{ asset(Config::get('factotum.files_url')) }}";
var media_url     = "{{ url('/admin/media') }}";
</script>
<?php
$fileName = $fileInfo[$key]['name'];

if( $file['type'] == 'image' ) {
	$thumbSrc = $thumbUrl . $fileName;
}
?>

<div class="col-sm-4 col-md-3 col-lg-2 img-row">

	<div class="thumbnail thumbnail-img text-center" data-id="{{ $fileName }}" id="img_thumbnail_{{ $key }}">
		@if ( $file['type'] == 'image' )
			<img id="{{ $fileName }}" src="{{ asset($thumbSrc) }}" alt="" class="pointer"
				 onclick="useFile('{{ $fileName }}')">
		@else
			<i class="fa fa-file-{{ $file['icon'] }} fa-5x"
			   style="height:200px;cursor:pointer;padding-top:60px;"
			   onclick="useFile('{{ $fileName }}')"></i>
		@endif
	</div>

	<div class="caption text-center">
		<div class="btn-group">
			<button type="button" onclick="useFile('{{ $fileName }}')" class="btn btn-default btn-xs">
				{{ str_limit($fileName, $limit = 10, $end = '...') }}
			</button>
			<button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown"
					aria-expanded="false">
				<span class="caret"></span>
				<span class="sr-only">Toggle Dropdown</span>
			</button>

			<!-- TODO: replace function with jquery calls -->
			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="javascript:rename('{{ $fileName }}')">
						<i class="fa fa-edit fa-fw"></i> Rename
					</a>
				</li>
				<li>
					<a href="javascript:download('{{ $fileName }}')">
						<i class="fa fa-download fa-fw"></i> Download
					</a>
				</li>
				<li class="divider"></li>

				@if ( $file['type'] == 'image' )
					<li>
						<a href="javascript:fileView('{{ $fileName }}')">
							<i class="fa fa-image fa-fw"></i> View
						</a>
					</li>
					<li>
						<a href="javascript:resizeImage('{{ $fileName }}')">
							<i class="fa fa-arrows fa-fw"></i> Resize
						</a>
					</li>
					<li>
						<a href="javascript:cropImage('{{ $fileName }}')">
							<i class="fa fa-crop fa-fw"></i> Crop
						</a>
					</li>
					<li class="divider"></li>
				@endif

				<li>
					<a href="javascript:trash('{{ $fileName }}')">
						<i class="fa fa-trash fa-fw"></i> Delete
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>

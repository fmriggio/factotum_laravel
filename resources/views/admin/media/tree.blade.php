<ul class="list-unstyled">
	<li style="margin-left: -10px;">
		<a class="pointer folder-item" data-id="{{ $mediaDir }}">
			<i class="fa fa-folder"></i> Media
		</a>
	</li>

	@foreach($mediaFolders as $key => $dirName)
		<li>
			<a class="pointer folder-item" data-id="{{ $dirName['long'] }}">
				<i class="fa fa-folder"></i> {{ $dirName['short'] }}
			</a>
		</li>
	@endforeach
</ul>

<div class="row">

	@if((sizeof($fileInfo) > 0) || (sizeof($directories) > 0))

		@foreach($directories as $key => $dir_name)
			@include('admin.media.folders')
		@endforeach

		@foreach($fileInfo as $key => $file)
			@include('admin.media.item')
		@endforeach

	@else
		<div class="col-md-12">
			<p>Folder empty</p>
		</div>
	@endif

</div>
function generateFieldName(str) {
	return str.replace(/[^a-z0-9]+/gi, '_').replace(/^-*|-*$/g, '').toLowerCase();
}

$('#field_label').on('keyup blur focusout', function() {
	var fieldName = generateFieldName($(this).val());
	$('#field_name, #field_name_hidden').val(fieldName);
});


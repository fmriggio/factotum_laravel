function generatePermalink(str) {
	return str.replace(/[^a-z0-9]+/gi, '-').replace(/^-*|-*$/g, '').toLowerCase();
}

$('#title').on('keyup blur focusout', function() {
	var permalink = generatePermalink($(this).val());
	$('#url').val(permalink);
});

$.datetimepicker.setLocale('it');

$(function() {
	$('input.date').datepicker({
		dateFormat: 'dd/mm/yy'
	});
	$('input.datetime').datetimepicker({
		format:'d/m/Y H:i:s',
	});
	$('textarea.wysiwyg').froalaEditor({ height: 300 });
});

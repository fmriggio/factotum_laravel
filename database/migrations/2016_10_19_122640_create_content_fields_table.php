<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('content_fields', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('content_type_id')->unsigned();
			$table->foreign('content_type_id')->references('id')->on('content_types')->onDelete('cascade');
			$table->string('name', 50);
			$table->string('label', 50);
			$table->string('type', 50);
			$table->integer('order_no')->unsigned()->nullable(true);
			$table->boolean('mandatory')->nullable(true);
			$table->string('hint', 255)->nullable(true);
			$table->text('options')->nullable(true);

			$table->integer('max_file_size')->unsigned()->nullable(true);
			$table->integer('min_width_size')->unsigned()->nullable(true);
			$table->integer('min_height_size')->unsigned()->nullable(true);
			$table->string('image_operation', 16)->nullable(true);
			$table->boolean('image_bw')->nullable(true);
			$table->string('allowed_types', 16)->nullable(true);
			$table->integer('thumb_width')->unsigned()->nullable(true);
			$table->integer('thumb_height')->unsigned()->nullable(true);

			$table->integer('linked_content_type_id')->unsigned()->nullable(true)->default(null);
			$table->foreign('linked_content_type_id')->references('id')->on('content_types')->onDelete('cascade');

			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('content_fields');
    }
}

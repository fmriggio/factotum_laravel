<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeContentFieldsResizeArrays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('content_fields', function (Blueprint $table) {
			$table->dropColumn('thumb_width');
			$table->dropColumn('thumb_height');
			$table->text('resizes')->nullable(true)->after('allowed_types');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('content_fields', function (Blueprint $table) {
			$table->integer('thumb_width')->unsigned()->nullable(true);
			$table->integer('thumb_height')->unsigned()->nullable(true);
			$table->dropColumn('resizes');
		});
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('content_values', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('content_field_id')->unsigned();
			$table->foreign('content_field_id')->references('id')->on('content_fields')->onDelete('cascade');
			$table->integer('content_id')->unsigned();
			$table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
			$table->string('field_name', 50);
			$table->text('value');

			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('content_values');
    }
}

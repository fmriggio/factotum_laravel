<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('roles')->insert([
			'role' => 'admin',
			'backend_access' => 1,
			'manage_content_types' => 1,
			'manage_users' => 1,
			'manage_content_categories' => 1,
		]);

		DB::table('roles')->insert([
			'role' => 'subscriber',
			'backend_access' => 0,
			'manage_content_types' => 0,
			'manage_users' => 0,
			'manage_content_categories' => 0,
		]);
    }
}
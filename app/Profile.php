<?php

namespace Factotum;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $fillable = [
		'first_name', 'last_name', 'user_id'
	];

	public function user() {
		return $this->belongsTo('Factotum\User');
	}
}

<?php

namespace Factotum;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
	protected $fillable = [
		'content_type',
	];

	public function content_fields() {
		return $this->hasMany('Factotum\ContentField');
	}
}

<?php

namespace Factotum\Http\Controllers\Admin\Content;

use Illuminate\Http\Request;

use Validator;

use Factotum\Content;


class UpdateController extends Controller
{
	public function edit( $id )
	{
		$content = Content::find($id);

		$this->_prepareContentFields( $content->content_type_id, $id );

		return view('admin.content.edit')
					->with('content', $content)
					->with('title', 'Update')
					->with('statuses', $this->statuses)
					->with('contentFields', $this->_contentFields)
					->with('contentsTree', $this->_contents)
					->with('additionalValues', $this->_additionalValues)
					->with('postUrl', url('/admin/content/update/' . $id) );
	}

	public function update(Request $request, $id)
	{
		$data = $request->all();
		$this->validator( $request, $data, $id )->validate();

		$content = Content::find($id);
		$this->_save( $request, $content );

		return redirect( 'admin/content/list/' . $content->content_type_id )->with('message', 'Successfully updated content!');
	}
}

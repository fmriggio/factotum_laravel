<?php

namespace Factotum\Http\Controllers\Admin\Content;

use Factotum\ContentType;
use Factotum\Content;
use Factotum\Library\Utility;

class ReadController extends Controller
{
	public function indexList($contentTypeId = null)
	{
		$contents = Content::treeObjects( $contentTypeId, 2 );

		return view('admin.content.list')
					->with('contentTypeId', $contentTypeId)
					->with('contents', $contents);
	}

	public function detail($id)
	{
		$contentType = ContentType::find($id);
		echo '<pre>';print_r($contentType->toArray());die;
	}
}


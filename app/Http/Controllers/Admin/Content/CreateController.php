<?php

namespace Factotum\Http\Controllers\Admin\Content;

use Factotum\ContentField;
use Factotum\Content;
use Factotum\Library\Utility;

use Illuminate\Http\Request;

class CreateController extends Controller
{

	public function create( $contentTypeId )
	{
		$this->_prepareContentFields( $contentTypeId );

		return view('admin.content.edit')
					->with('title', 'Create')
					->with('postUrl', url('/admin/content/store/' . $contentTypeId) )
					->with('statuses', $this->statuses)
					->with('contentFields', $this->_contentFields)
					->with('contentsTree', $this->_contents);
	}

	public function store( $contentTypeId, Request $request )
	{
		$data = $request->all();
		$data['content_type_id'] = $contentTypeId;
		$this->validator( $request, $data )->validate();

		$content = new Content;
		$content->content_type_id = $contentTypeId;

		$this->_save( $request, $content );

		return redirect( 'admin/content/list/' . $contentTypeId )->with('message', 'Successfully created content!');
	}

}


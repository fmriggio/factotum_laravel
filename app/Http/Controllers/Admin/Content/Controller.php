<?php

namespace Factotum\Http\Controllers\Admin\Content;

use Factotum\Http\Controllers\Admin\Controller as MainAdminController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Validator;
use Image;

use Factotum\Library\Utility;
use Factotum\ContentType;
use Factotum\ContentField;
use Factotum\Content;
use Factotum\Media;


class Controller extends MainAdminController
{
	protected $statuses = array(
		'draft'   => 'Draft',
		'publish' => 'Publish'
	);

	protected $_contentType;
	protected $_contentFields;
	protected $_contents;
	protected $_additionalValues;

	protected function _prepareContentFields( $contentTypeID, $contentID = null )
	{
		$this->_contentType   = ContentType::find( $contentTypeID );
		$this->_contents      = Content::treeArray( $this->_contentType->id );
		$this->_contentFields = ContentField::where( 'content_type_id', '=', $contentTypeID )->get();

		if ( $contentID ) {
			$this->_additionalValues = DB::table( $this->_contentType->content_type )
												->where( 'content_type_id', $this->_contentType->id )
												->where( 'content_id', $contentID )
												->first();
		}

		if ( $this->_contentFields->count() > 0 ) {
			foreach ( $this->_contentFields as $field ) {
				if ( $contentID ) {

					if ( $field->type == 'file_upload' || $field->type == 'image_upload' ) {
						if ( isset($this->_additionalValues->{$field->name}) ) {
							$media = Media::find( $this->_additionalValues->{$field->name} );
							$this->_additionalValues->{$field->name} = $media->toArray();
						}
					} else if ( $field->type == 'gallery' ) {
						if ( isset($this->_additionalValues->{$field->name}) ) {
							$media = Media::findMany( Utility::convertOptionsTextToArray( $this->_additionalValues->{$field->name} ) );
							$this->_additionalValues->{$field->name} = $media->toArray();
						}
					}
				}

				if ( $field->type == 'linked_content' || $field->type == 'multiple_linked_content' ) {
					$field->options = Content::treeArray( $this->_contentType->id );
				}
			}
		}


	}

	protected function _save( Request $request, $content )
	{
		$data = $request->all();

		$user = Auth::user();

		$content->user_id   = $user->id;
		$content->status    = $data['status'];
		if ( $data['parent_id'] != '') {
			$content->parent_id = $data['parent_id'];
		}
		$content->title     = $data['title'];
		$content->url       = $data['url'];
		$content->content   = $data['content'];

		// TODO: lang
		$content->lang = 'en';
		$content->save();

		$contentType   = ContentType::find($content->content_type_id);
		$contentFields = ContentField::where( 'content_type_id', '=', $contentType->id )->get();
		if ( $contentFields->count() > 0 ) {

			$additionalValuesExists = DB::table( $contentType->content_type )
										->where( 'content_type_id', $contentType->id )
										->where( 'content_id', $content->id )
										->first();

			$additionalValues = array(
				'content_type_id' => $contentType->id,
				'content_id'      => $content->id
			);

			$this->contentDir = 'media/' . $content->id;

			foreach ( $contentFields as $field ) {

				// Multioptions fields
				if ( $field->type == 'multicheckbox' || $field->type == 'multiselect' || $field->type == 'multiple_linked_content' ) {
					$data[ $field->name ] = Utility::convertOptionsArrayToText( $data[ $field->name ] );
				}

				// Date-time fields
				if ( $field->type == 'date' ) {
					$data[ $field->name ] = Utility::convertHumanDateToIso( $data[ $field->name ] );
				}
				if ( $field->type == 'datetime' ) {
					$data[ $field->name ] = Utility::convertHumanDateTimeToIso( $data[ $field->name ] );
				}

				// Save generic file upload (file or image)
				$file = null;
				if ( $field->type == 'file_upload' || $field->type == 'image_upload' ) {

					if ($request->hasFile( $field->name )) {
						if ($request->file( $field->name )->isValid()) {
							$file = $request->file( $field->name );
							$data[ $field->name ] = $this->_saveMedia( $file, $field );
						}
					} else {
						$data[ $field->name ] = $data[ $field->name . '_hidden' ];
					}
				}

				if ( $field->type == 'gallery' ) {
					$tmp = array();

					if ( isset( $data[ $field->name . '_hidden' ] ) && $data[ $field->name . '_hidden' ] != '' ) {
						$tmp = Utility::convertOptionsTextToArray( $data[ $field->name . '_hidden' ] );
					}

					if ($request->hasFile( $field->name )) {
						$files = $request->file( $field->name );
						if ( count($files) > 0 ) {
							foreach ( $files as $file ) {
								if ( $file->isValid() ) {
									$tmp[] = $this->_saveMedia( $file, $field );
								}
							}
							$data[ $field->name ] = join(';', $tmp);
						}

					} else {
						$data[ $field->name ] = $data[ $field->name . '_hidden' ];
					}
				}

				if ( $data[ $field->name ] != '') {
					$additionalValues[ $field->name ] = $data[ $field->name ];
				}
			}

			if ( $additionalValuesExists ) {
				DB::table( $contentType->content_type )
					->where( 'id', $additionalValuesExists->id )
					->update( $additionalValues );
			} else {
				DB::table( $contentType->content_type )->insert( $additionalValues );
			}

		}

		return $content;
	}

	private function _saveMedia( $file, $field )
	{
		$filename = $file->getClientOriginalName();

		$filename = $this->_filenameAvailable($filename, $filename);

		$media = new Media;
		$media->filename  = $filename;
		$media->mime_type = $file->getMimeType();
		$media->save();

		$mediaDir = 'media/' . $media->id;
		Storage::makeDirectory( $mediaDir );

		$path = $file->storeAs( $mediaDir, $filename );

		$media->url = $path;
		$media->save();

		if ( $file && ( $field->type == 'image_upload' || $field->type == 'gallery' ) ) {
			$this->_saveImage( $field, $media->url );
		}

		return $media->id;
	}

	private function _filenameAvailable($filename, $origFilename, $counter = '')
	{
		$mediaExist = Media::where('filename', $filename)->get();
		if ( $mediaExist->count() > 0 ) {
			if (!$counter) {
				$counter = 1;
			} else {
				$counter++;
			}
			$ext      = substr( $origFilename, strlen($origFilename) -3 , 3);
			$filename = substr( $origFilename, 0, -4 ) . '-' . $counter . '.' . $ext;
			return $this->_filenameAvailable($filename, $origFilename, $counter);
		}
		return $filename;
	}

	private function _saveImage( $field, $filename )
	{
		if ( $field->image_bw ) {
			$image = Image::make( $filename )->greyscale();
		} else {
			$image = Image::make( $filename );
		}

		$ext      = substr( $filename, strlen($filename) - 3, 3 );
		$filename = substr( $filename, 0, -4 );

		$operation = $field->image_operation;
		if ($field->resizes) {
			$resizes = Utility::convertOptionsTextToAssocArray( $field->resizes );
			foreach ($resizes as $width => $height) {
				$newFilename = $filename . '-' . $width .'x' . $height . '.' . $ext;
				if ($operation == 'resize') {
					$image->resize( $width, null, function ($constraint) {
						$constraint->aspectRatio();
					});
					$image->save( $newFilename );
				} else if ($operation == 'crop') {
					$image->crop( $width, $height );
				} else if ($operation == 'fit') {
					$image->fit( $width, $height, function ($constraint) {
						$constraint->upsize();
					});
					$image->save( $newFilename );
				}
			}
		}

		return $image;
	}

	protected function validator(Request $request, array $data, $id = null)
	{
		$rules = array(
			'title'  => 'required',
			'url'    => 'required',
			'status' => 'required'
		);

		if ($id) {
			$content = Content::find($id);
			if ( $data['url'] != $content->url ) {
				$rules['url'] .= '|unique:contents';
			}

			$contentFields = ContentField::where( 'content_type_id', '=', $content->content_type_id )->get();

		} else {
			$rules['content_type_id'] = 'required';
			$rules['url'] .= '|unique:contents';

			$contentFields = ContentField::where( 'content_type_id', '=', $data['content_type_id'] )->get();
		}


		// Additional Fields
		if ( $contentFields->count() > 0 ) {
			foreach ( $contentFields as $field ) {
				$tmp = array();

				if ( $field->mandatory ) {

					if (count($data) > 0 && ( $field->type == 'file_upload' || $field->type == 'image_upload'  || $field->type == 'gallery' )) {
						if ( !isset($data[ $field->name . '_hidden' ]) ||
							 (isset($data[ $field->name . '_hidden' ]) && $data[ $field->name . '_hidden' ] == '' ) ) {
							$tmp[] = 'required';
						}
					} else {
						$tmp[] = 'required';
					}
				}

				if ( $field->type == 'file_upload' || $field->type == 'image_upload' || $field->type == 'gallery' ) {
					$tmp[] = 'max:' . $field->max_file_size;
					if ($field->allowed_types != '*') {
						$field->allowed_types = str_replace('jpg', 'jpeg', $field->allowed_types);
						$tmp[] = 'mimes:' . $field->allowed_types;
					}
				}

				if ( $field->type == 'image_upload' || $field->type == 'gallery' ) {
					$tmp[] = 'dimensions:min_width=' . $field->min_width_size . ',min_height=' . $field->min_height_size;
				}


				if ( $field->type == 'gallery' ) {
					$nbr = count( $request->file( $field->name ) ) - 1;
					foreach(range(0, $nbr) as $index) {
						$rules[ $field->name .  '.' . $index ] = join( '|', $tmp );
					}
				} else {
					$rules[ $field->name ] = join( '|', $tmp );
				}

			}
		}

		return Validator::make($data, $rules);
	}
}

<?php

namespace Factotum\Http\Controllers\Admin\ContentType;

use Factotum\Http\Controllers\Admin\Controller as MainAdminController;

use Illuminate\Http\Request;
use Validator;


class Controller extends MainAdminController
{
	protected function _save( Request $request, $contentType )
	{
		$data = $request->all();

		$contentType->content_type = strtolower($data['content_type']);
		$contentType->save();

		return $contentType;
	}

	protected function validator(array $data)
	{
		$rules = array(
			'content_type' => 'required|max:32|unique:content_types|not_in:' . join(',', config('factotum.prohibited_content_types') )
		);
		return Validator::make($data, $rules);
	}
}

<?php

namespace Factotum\Http\Controllers\Admin\ContentType;

use Factotum\ContentType;
use Factotum\Role;

use Illuminate\Http\Request;
use Validator;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateController extends Controller
{

	public function create()
	{
		return view('admin.content_type.edit')
					->with('title', 'Create')
					->with('postUrl', url('/admin/content-type/store') );
	}

	public function store(Request $request)
	{
		$this->validator($request->all())->validate();

		$contentType = new ContentType;
		$this->_save( $request, $contentType );

		Schema::create( $contentType->content_type, function (Blueprint $table) {
			$table->increments('id');
			$table->integer('content_type_id')->unsigned();
			$table->foreign('content_type_id')->references('id')->on('content_types')->onDelete('cascade');
			$table->integer('content_id')->unsigned();
			$table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
		});

		// TODO: add automatically capabilities to all the admin role
		return redirect('admin/content-type/list')->with('message', 'Successfully created content type!');
	}

}

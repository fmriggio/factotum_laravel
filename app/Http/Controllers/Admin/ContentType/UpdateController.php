<?php

namespace Factotum\Http\Controllers\Admin\ContentType;

use Factotum\ContentType;

use Illuminate\Http\Request;
use Validator;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class UpdateController extends Controller
{
	public function edit($id)
	{
		$contentType = ContentType::find($id);

		return view('admin.content_type.edit')
					->with('contentType', $contentType)
					->with('title', 'Update')
					->with('postUrl', url('/admin/content-type/update/' . $id ) );
	}

	public function update(Request $request, $id)
	{
		$this->validator($request->all(), $id)->validate();

		$contentType = ContentType::find($id);
		$oldContentType = $contentType->content_type;
		$contentType = $this->_save( $request, $contentType );
		Schema::rename( $oldContentType, $contentType->content_type);

		return redirect('admin/content-type/list')->with('message', 'Successfully updated content type!');
	}
}

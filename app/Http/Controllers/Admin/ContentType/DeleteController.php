<?php

namespace Factotum\Http\Controllers\Admin\ContentType;

use Factotum\ContentType;
use Factotum\ContentField;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class DeleteController extends Controller
{
	public function delete($id)
	{
		// TODO: remove content relative to this content type
		$contentType = ContentType::find($id);
		$deletedRows = ContentField::where('content_type_id', $id)->delete();
		if ($deletedRows >= 0) {
			$deletedRows = ContentType::destroy($id);
			if ($deletedRows == 1) {
				Schema::drop($contentType->content_type);
			}
		}
		return redirect('/admin/content-type/list')->with('message', 'Content Type deleted!');
	}
}

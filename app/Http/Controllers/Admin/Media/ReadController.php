<?php

namespace Factotum\Http\Controllers\Admin\Media;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;


class ReadController extends Controller
{

	public function show()
	{
		$workingDir = '/';
		$workingDir .= Config::get('factotum.shared_folder_name');

		$extensionNotFound = ! extension_loaded('gd') && ! extension_loaded('imagick');

		return view('admin.media.index')
					->with( 'workingDir', $workingDir )
					//->with( 'imagesMimeTypes', $imagesMimeTypes )
					//->with( 'filesMimeTypes', $filesMimeTypes )
					//->with( 'file_type', $this->fileType )
					->with( 'extensionNotFound', $extensionNotFound );

	}

	private function checkDefaultFolderExists($type = 'media')
	{
		$path = $this->getPath($type);

		if ( !File::exists($path) ) {
			File::makeDirectory($path, $mode = 0777, true, true);
		}
	}

	/**
	 * Get the images to load for a selected folder
	 *
	 * @return mixed
	 */
	public function getItems()
	{
		$view = $this->getView();
		$path = parent::getPath();

		$files           = File::files( $path );
		$fileInfo        = $this->getFileInfos( $files );
		$directories     = parent::getDirectories( $path );
		$thumbUrl        = parent::getUrl( 'thumb' );

		return view( $view )
			->with( 'imagesMimeTypes', $this->imagesMimeTypes )
			->with( 'filesMimeTypes', $this->filesMimeTypes )
			->with( 'fileInfo', $fileInfo )
			->with( 'directories', $directories )
			->with( 'thumbUrl', $thumbUrl );
	}

	// Get files infos
	private function getFileInfos( $files )
	{
		$fileInfo = [];

		foreach ($files as $key => $file) {

			$fileName    = parent::getFileName($file)['short'];
			$fileCreated = filemtime($file);
			$fileSize    = number_format((File::size($file) / 1024), 2, '.', '');

			if ($fileSize > 1024) {
				$fileSize = number_format(($fileSize / 1024), 2, '.', '') . ' Mb';
			} else {
				$fileSize = $fileSize . ' Kb';
			}

			$fileMime = File::mimeType($file);

			if ( in_array( $fileMime,  $this->imagesMimeTypes ) ) {
				$fileType = 'image';
			} else if ( in_array( $fileMime,  $this->filesMimeTypes ) ) {
				$fileType = 'file';
			} else {
				$fileType = 'unknown';
			}

			$fileInfo[$key] = [
				'name'      => $fileName,
				'size'      => $fileSize,
				'created'   => $fileCreated,
				'mime'      => $fileMime,
				'type'      => $fileType,
				'icon'      => str_replace('/', '-', $fileMime)
			];
		}

		return $fileInfo;
	}

	private function getView()
	{

		if (Input::get('show_list') == 1) {
			return 'admin.media.list-view';
		} else {
			return 'admin.media.grid-view';
		}
	}

}


<?php
namespace Factotum\Http\Controllers\Admin\Media;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Event;

use Factotum\Events\ImageWasDeleted;

/**
 * Class CropController
 * @package Unisharp\Laravelfilemanager\controllers
 */
class DeleteController extends Controller {

    /**
     * Delete image and associated thumbnail
     *
     * @return mixed
     */
    public function getDelete()
    {
        $nameToDelete = Input::get('items');

        $filePath = parent::getPath('directory');

        $fileToDelete  = $filePath . $nameToDelete;
        $thumbToDelete = parent::getPath('thumb') . $nameToDelete;

        if (!File::exists($fileToDelete)) {
            return $fileToDelete . ' not found!';
        }

        if (File::isDirectory($fileToDelete)) {
            if (sizeof(File::files($fileToDelete)) != 0) {
                return 'Error on delete the file!';
            }

            File::deleteDirectory($fileToDelete);

            return 'OK';
        }

        File::delete($fileToDelete);
        Event::fire(new ImageWasDeleted($fileToDelete));
        
        if ('Images' === $this->fileType) {
            File::delete( $thumbToDelete );
        }

        return 'OK';
    }

}

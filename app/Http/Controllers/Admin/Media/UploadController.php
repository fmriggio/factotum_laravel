<?php
namespace Factotum\Http\Controllers\Admin\Media;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;

use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Factotum\Events\ImageWasUploaded;

/**
 * Class UploadController
 * @package Unisharp\Laravelfilemanager\controllers
 */
class UploadController extends Controller {

    private $defaultFileTypes  = ['application/pdf'];
    private $defaultImageTypes = ['image/jpeg', 'image/png', 'image/gif'];

    /**
     * Upload an image/file and (for images) create thumbnail
     *
     * @param UploadRequest $request
     * @return string
     */
    public function upload()
    {
		try {
			$res = $this->uploadValidator();
			if (true !== $res) {
				return 'Invalid upload request';
			}
		} catch (\Exception $e) {
			return $e->getMessage();
		}

		$file = Input::file('upload');

		$newFilename = $this->getNewName($file);

		$destPath = parent::getPath('directory');

		if (File::exists($destPath . $newFilename)) {
			return 'A file with this name already exists!';
		}

		$file->move($destPath, $newFilename);

		if ('Images' === $this->fileType) {
			$this->makeThumb($destPath, $newFilename);
		}

		Event::fire(new ImageWasUploaded(realpath($destPath.'/'.$newFilename)));

		// Upload via ckeditor 'Upload' tab
		if ( !Input::has('show_list') ) {
			return $this->useFile($newFilename);
		}

        return 'OK';
    }

	private function uploadValidator()
	{
		// when uploading a file with the POST named "upload"
		$expectedFileType = $this->fileType;
		$isValid = false;

		$file = Input::file('upload');

		if (empty($file)) {
			throw new \Exception( 'You must choose a file!' );
		} elseif (!$file instanceof UploadedFile) {
			throw new \Exception( 'The uploaded file should be an instance of UploadedFile' );
		} elseif ($file->getError() == UPLOAD_ERR_INI_SIZE) {
			$max_size = ini_get('upload_max_filesize');
			throw new \Exception( 'File size exceeds server limit! (maximum size: :max)', ['max' => $max_size] );
		} elseif ($file->getError() != UPLOAD_ERR_OK) {
			dd('File failed to upload. Error code: ' . $file->getError());
		}

		$mimeType = $file->getMimeType();

		// TODO: not working with files that are not images
		if ($expectedFileType === 'Files') {
			$configName = 'factotum.valid_file_mimetypes';
			$validMimeTypes = Config::get($configName, $this->defaultFileTypes);
		} else {
			$configName = 'factotum.valid_image_mimetypes';
			$validMimeTypes = Config::get($configName, $this->defaultImageTypes);
		}

		$validFilesMimeTypes  = Config::get( 'factotum.valid_file_mimetypes', $this->defaultFileTypes );
		$validImagesMimeTypes = Config::get( 'factotum.valid_image_mimetypes', $this->defaultImageTypes );
		$validMimeTypes = array_merge( $validFilesMimeTypes, $validImagesMimeTypes );

		if (!is_array($validMimeTypes)) {
			throw new \Exception('Config : ' . $configName . ' is not set correctly');
		}

		if (in_array($mimeType, $validImagesMimeTypes)) {
			$isValid = true;
			$this->fileType = 'Images';
		} else if (in_array($mimeType, $validFilesMimeTypes)) {
			$isValid = true;
			$this->fileType = 'Files';
		}

		if (false === $isValid) {
			throw new \Exception( 'Unexpected MimeType: ' . $mimeType);
		}

		return $isValid;
	}

    private function getNewName($file)
    {
		$newFilename = trim(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));

        if (Config::get('factotum.rename_file') === true) {
            $newFilename = uniqid();
        } elseif (Config::get('factotum.alphanumeric_filename') === true) {
			$newFilename = preg_replace('/[^A-Za-z0-9\-\']/', '_', $newFilename);
        }

		return $newFilename . '.' . $file->getClientOriginalExtension();
    }

    private function makeThumb($destPath, $newFilename)
    {
		$thumbFolderName = Config::get('factotum.thumb_folder_name');

        if (!File::exists($destPath . $thumbFolderName)) {
            File::makeDirectory($destPath . $thumbFolderName);
        }

        $thumbImg = Image::make($destPath . $newFilename);
        $thumbImg->fit(200, 200)
            	 ->save($destPath . $thumbFolderName . '/' . $newFilename);
        unset($thumbImg);
    }

	// TODO: move on client side!
    private function useFile($newFilename)
    {
        $file = parent::getUrl('directory') . $newFilename;

        return "<script type='text/javascript'>

        function getUrlParam(paramName) {
            var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
            var match = window.location.search.match(reParam);
            return ( match && match.length > 1 ) ? match[1] : null;
        }

        var funcNum = getUrlParam('CKEditorFuncNum');

        var par = window.parent,
            op = window.opener,
            o = (par && par.CKEDITOR) ? par : ((op && op.CKEDITOR) ? op : false);

        if (op) window.close();
        if (o !== false) o.CKEDITOR.tools.callFunction(funcNum, '$file');
        </script>";
    }

}

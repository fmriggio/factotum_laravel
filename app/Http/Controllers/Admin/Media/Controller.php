<?php

namespace Factotum\Http\Controllers\Admin\Media;

use Factotum\Http\Controllers\Admin\Controller as MainAdminController;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class Controller extends MainAdminController
{

	/**
	 * @var
	 */
	public $fileLocation = null;
	public $dirLocation = null;
	public $fileType = null;
	protected $imagesMimeTypes = array();
	protected $filesMimeTypes = array();


	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->fileType = Input::get('type', 'Images');

		$this->dirLocation  = Config::get('factotum.media_url');
		$this->fileLocation = Config::get('factotum.media_dir');

		$this->imagesMimeTypes = Config::get('factotum.valid_image_mimetypes');
		$this->filesMimeTypes  = Config::get('factotum.valid_file_mimetypes');

		$path = base_path() . '/' . $this->fileLocation;

		if ( !File::exists($path) ) {
			// TODO: change permission // 0777 is not secure!!!
			File::makeDirectory($path, $mode = 0777, true, true);
		}
	}


	// TODO: problably need to remove
	public function getUserSlug()
	{
		return empty(auth()->user()) ? '' : \Auth::user()->user_field;
	}


	public function getPath( $type = null )
	{
		$path = base_path() . '/' . $this->fileLocation;
		$path = $this->formatLocation($path, $type);
		return $path;
	}


	public function getUrl( $type = null )
	{
		$url = $this->dirLocation;
		$url = $this->formatLocation($url, $type);
		$url = str_replace('\\','/',$url);
		return $url;
	}


	public function getDirectories($path)
	{
		$thumbFolderName = Config::get('factotum.thumb_folder_name');
		//$allDirectories = Storage::allDirectories( $path );
		$allDirectories = File::directories($path);

		$arrDir = [];

		foreach ($allDirectories as $directory) {
			$dirName = $this->getFileName($directory);

			if ( $dirName['short'] !== $thumbFolderName ) {
				$arrDir[] = $dirName;
			}
		}

		return $arrDir;
	}


	public function getFileName($file)
	{
		$dirStart = strpos($file, $this->fileLocation);
		$workingDirStart = $dirStart + strlen($this->fileLocation);
		$filePath = substr($file, $workingDirStart);

		$arrDir = explode('/', $filePath);
		$arrFilename['short'] = end($arrDir);
		//$arrFilename['long'] = '/' . $lfm_file_path;
		$arrFilename['long'] = $filePath;

		return $arrFilename;
	}

	private function formatLocation( $location, $type = null )
	{
		if ($type === 'media') {
			return $location . Config::get('factotum.media_folder_name');
		}

		$workingDir = Input::get('working_dir');

		// remove first slash
		if (substr($workingDir, 0, 1) === '/') {
			$workingDir = substr($workingDir, 1);
		}

		$location .= $workingDir;

		if ($type === 'directory' || $type === 'thumb') {
			$location .= '/';
		}

		// if user is inside thumbs folder there is no need
		// to add thumbs substring to the end of $location
		$inThumbFolder = preg_match( '/' . Config::get('factotum.thumb_folder_name') . '$/i', $workingDir );

		if ( $type === 'thumb' && !$inThumbFolder ) {
			$location .= Config::get('factotum.thumb_folder_name') . '/';
		}

		return $location;
	}
}

<?php

namespace Factotum\Http\Controllers\Admin\Media;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;


class FolderController extends Controller
{

	/**
	 * Get list of folders as json to populate treeview
	 *
	 * @return mixed
	 */
	public function getFolders()
	{
		$mediaPath       = parent::getPath('media');
		$customMediaPath = parent::getFileName($mediaPath);
		$mediaFolders   = parent::getDirectories($mediaPath);

		return view('admin.media.tree')
					->with('mediaDir', $customMediaPath['long'])
					->with('mediaFolders', $mediaFolders);

	}

	/**
	 * Add a new folder
	 *
	 * @return mixed
	 */
	public function addFolder()
	{
		$folderName = trim(Input::get('name'));

		$path = parent::getPath('directory') . $folderName;

		if (File::exists($path)) {
			return 'A folder with this name already exists!';
		} elseif (empty($folderName)) {
			return 'Folder name cannot be empty!';
		} elseif (Config::get('lfm.alphanumeric_directory') && preg_match('/[^\w-]/i', $folderName)) {
			return 'Only alphanumeric folder names are allowed!';
		} else {
			File::makeDirectory($path, $mode = 0777, true, true);
			return 'OK';
		}
	}

}
<?php

namespace Factotum\Http\Controllers\Admin\User;

use Factotum\Http\Controllers\Admin\Controller as MainAdminController;
use Factotum\User;

use Illuminate\Http\Request;
use Validator;


class Controller extends MainAdminController
{
	protected function _save( Request $request, $user, $profile )
	{
		$data = $request->all();

		$user->email = $data['email'];
		if ( isset($data['password']) ) {
			$user->password = bcrypt($data['password']);
		}
		$user->role_id = $data['role_id'];
		$user->save();

		$profile->first_name = $data['first_name'];
		$profile->last_name  = $data['last_name'];
		$profile->user_id    = $user->id;
		$profile->save();

		return $user;
	}

	protected function validator(array $data, $userId = false)
	{
		$rules = array(
			'first_name'     => 'required|max:64',
			'last_name'      => 'required|max:64',
			'role_id'        => 'required',
			'email'          => 'required|email|max:128',
		);
		if (!$userId) {
			$rules['email']    = 'required|email|max:128|unique:users';
			$rules['password'] = 'required|min:6|confirmed';
		} else {
			$user = User::find($userId);
			if ($data['email'] != $user->email) {
				$rules['email'] = 'required|email|max:128|unique:users';
			}
			if ($data['password'] != '') {
				$rules['password'] = 'required|min:6|confirmed';
			}
		}
		return Validator::make($data, $rules);
	}
}

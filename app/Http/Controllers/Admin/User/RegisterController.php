<?php

namespace Factotum\Http\Controllers\Admin\User;

use Factotum\Http\Controllers\Admin\Controller;
use Factotum\User;
use Factotum\Profile;
use Factotum\Role;

use Validator;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;



class RegisterController extends Controller
{
	use RedirectsUsers;

	protected $redirectTo = '/admin/auth/login';

	public function index()
	{
		$roles = Role::all();
		return view('admin.user.register')->with('roles', $roles);
	}

	public function register(Request $request)
	{
		$this->validator($request->all())->validate();
		event(new Registered($user = $this->create($request->all())));
		$this->guard()->login($user);
		return redirect($this->redirectPath());
	}


	protected function validator(array $data)
	{
		return Validator::make($data, [
			'first_name'     => 'required|max:64',
			'last_name'      => 'required|max:64',
			'role_id'        => 'required',
			'email'          => 'required|email|max:128|unique:users',
			'password'       => 'required|min:6|confirmed',
		]);
	}

	protected function create(array $data)
	{
		$user = User::create([
			'email'    => $data['email'],
			'password' => bcrypt($data['password']),
			'role_id'  => $data['role_id']
		]);

		$createProfile = Profile::create([
			'first_name' => $data['first_name'],
			'last_name'  => $data['last_name'],
			'user_id'    => $user->id
		]);
		return $user;
	}

}

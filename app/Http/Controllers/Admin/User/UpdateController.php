<?php

namespace Factotum\Http\Controllers\Admin\User;

use Factotum\User;
use Factotum\Role;

use Illuminate\Http\Request;
use Validator;


class UpdateController extends Controller
{
	public function edit($id)
	{
		$user = User::find($id);
		$roles = Role::all();

		return view('admin.user.edit')
			->with('user', $user)
			->with('title', 'Create')
			->with('postUrl', url('/admin/user/update/' . $id ) )
			->with('roles', $roles);
	}

	public function update(Request $request, $id)
	{
		if ($request->get('password') == '') {
		}

		$this->validator($request->all(), $id)->validate();

		$user = User::find($id);
		$profile = $user->profile;

		$this->_save( $request, $user, $profile );

		return redirect('admin/user/list')->with('message', 'Successfully updated user!');
	}
}

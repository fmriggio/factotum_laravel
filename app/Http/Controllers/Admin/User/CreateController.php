<?php

namespace Factotum\Http\Controllers\Admin\User;

use Factotum\User;
use Factotum\Role;
use Factotum\Profile;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Validator;


class CreateController extends Controller
{

	public function create()
	{
		$roles = Role::all();
		return view('admin.user.edit')
					->with('title', 'Create')
					->with('postUrl', url('/admin/user/store') )
					->with('roles', $roles);
	}

	public function store(Request $request)
	{
		$this->validator($request->all())->validate();

		$user = new User;
		$profile = new Profile;
		$user = $this->_save( $request, $user, $profile );

		event(new Registered($user));

		return redirect('admin/user/list')
			->with('message', 'Successfully created content type!');
	}

}

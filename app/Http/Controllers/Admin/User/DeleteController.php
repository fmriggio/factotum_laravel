<?php

namespace Factotum\Http\Controllers\Admin\User;

use Factotum\User;

class DeleteController extends Controller
{
	// TODO
    public function delete($id)
	{
		User::destroy($id);
		return redirect('/admin/user/list')->with('message', 'User deleted!');
	}
}

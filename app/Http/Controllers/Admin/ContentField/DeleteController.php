<?php

namespace Factotum\Http\Controllers\Admin\ContentField;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use Factotum\ContentType;
use Factotum\ContentField;
use Factotum\Library\Utility;


class DeleteController extends Controller
{
	public function delete($id)
	{
		$contentField = ContentField::find($id);
		$contentType  = ContentType::find($contentField->content_type_id);

		ContentField::destroy($id);

		// Alter relative table
		Schema::table( $contentType->content_type, function (Blueprint $table)  use ( $contentField ) {
			$table->dropColumn( $contentField->name );
		});

		// TODO: delete all the values connected to this value.

		// TODO: if delete a value that has a media connected, drop the media
		return redirect('/admin/content-field/list')->with('message', 'Content Field deleted!');
	}
}

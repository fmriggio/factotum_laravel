<?php

namespace Factotum\Http\Controllers\Admin\ContentField;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Tests\Exception\UnsupportedMediaTypeHttpExceptionTest;
use Validator;

use Factotum\Library\Utility;

use Factotum\ContentField;
use Factotum\ContentType;



class UpdateController extends Controller
{
	public function edit($contentTypeId, $id)
	{
		$contentField = ContentField::find($id);
		$contentTypes = ContentType::all();

		if ( $contentField->options != '' ) {
			$contentField->options = Utility::convertOptionsTextToAssocArray( $contentField->options );
		}
		if ( $contentField->resizes != '' ) {
			$contentField->resizes = Utility::convertOptionsTextToAssocArray( $contentField->resizes );
		}

		return view('admin.content_field.edit')
					->with('contentField', $contentField)
					->with('title', 'Update')
					->with('fieldTypes', $this->fieldTypes)
					->with('imageOperations', $this->imageOperations)
					->with('contentTypes', $contentTypes)
					->with('postUrl', url('/admin/content-field/update/' . $contentTypeId . '/' . $id) );
	}

	public function update(Request $request, $contentTypeId, $id)
	{
		$oldContentField = ContentField::find( $id );

		$data = $request->all();
		$data['content_type_id'] = $contentTypeId;
		$this->validator($data, $id)->validate();

		$contentField = ContentField::find($id);
		$contentField = $this->_save( $request, $contentField );

		$contentType = ContentType::find($contentField->content_type_id);


		// Alter relative table
		Schema::table( $contentType->content_type, function (Blueprint $table)  use ($oldContentField, $contentField, $data) {

			// TODO:check if is working
			if ( $contentField->name != $oldContentField->name ) {
				$table->renameColumn( $oldContentField->name, $contentField->name )->change();
			}

			if ( $contentField->type == 'text' ||
				 $contentField->type == 'select' || $contentField->type == 'multiselect' ||
				 $contentField->type == 'checkbox' || $contentField->type == 'multicheckbox' ||
				 $contentField->type == 'radio' ||
				 $contentField->type == 'file_upload' || $contentField->type == 'image_upload' ||
				 $contentField->type == 'multiple_linked_content' ) {
				$table->string( $contentField->name, 255 )->nullable( !$contentField->mandatory )->change();
			}

			if ( $contentField->type == 'date' ) {
				$table->date( $contentField->name )->nullable( !$contentField->mandatory )->change();
			}

			if ( $contentField->type == 'datetime' ) {
				$table->dateTime( $contentField->name )->nullable( !$contentField->mandatory )->change();
			}

			if ( $contentField->type == 'textarea' ||
				 $contentField->type == 'wysiwyg' ||
				 $contentField->type == 'gallery' ) {
				$table->text( $contentField->name )->nullable( !$contentField->mandatory )->change();
			}

			if ( $contentField->type == 'linked_content' ) {
				$table->integer( $contentField->name )->nullable( !$contentField->mandatory )->change();
			}
		});

		// TODO: update all the values if the name changes

		return redirect('admin/content-field/list')->with('message', 'Successfully updated content field!');
	}
}

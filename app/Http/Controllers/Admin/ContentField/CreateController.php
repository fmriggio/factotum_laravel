<?php

namespace Factotum\Http\Controllers\Admin\ContentField;

use Factotum\ContentType;
use Factotum\ContentField;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Validator;


class CreateController extends Controller
{

	public function create( $contentTypeId )
	{
		$contentTypes = ContentType::all();

		return view('admin.content_field.edit')
					->with('title', 'Create')
					->with('fieldTypes', $this->fieldTypes)
					->with('imageOperations', $this->imageOperations)
					->with('contentTypes', $contentTypes)
					->with('postUrl', url('/admin/content-field/store/' . $contentTypeId) );
	}

	public function store( $contentTypeId, Request $request )
	{
		$data = $request->all();
		$data['content_type_id'] = $contentTypeId;
		$this->validator($data)->validate();

		$contentField = new ContentField;
		$contentField->content_type_id = $contentTypeId;

		$this->_save( $request, $contentField );

		$contentType = ContentType::find($contentField->content_type_id);

		// Alter relative table
		Schema::table( $contentType->content_type, function (Blueprint $table)  use ($contentField) {
			if ( $contentField->type == 'text' ||
				$contentField->type == 'select' || $contentField->type == 'multiselect' ||
				$contentField->type == 'checkbox' || $contentField->type == 'multicheckbox' ||
				$contentField->type == 'radio' ||
				$contentField->type == 'file_upload' || $contentField->type == 'image_upload' ||
				$contentField->type == 'multiple_linked_content' ) {
				$table->string( $contentField->name, 255 )->nullable( !$contentField->mandatory );
			}

			if ( $contentField->type == 'date' ) {
				$table->date( $contentField->name )->nullable( !$contentField->mandatory );
			}

			if ( $contentField->type == 'datetime' ) {
				$table->dateTime( $contentField->name )->nullable( !$contentField->mandatory );
			}

			if ( $contentField->type == 'textarea' || $contentField->type == 'wysiwyg' ||
				$contentField->type == 'gallery' ) {
				$table->text( $contentField->name, 255 )->nullable( !$contentField->mandatory );
			}

			if ( $contentField->type == 'linked_content' ) {
				$table->integer( $contentField->name )->nullable( !$contentField->mandatory );
			}
		});

		return redirect('admin/content-field/list')->with('message', 'Successfully created content field!');
	}

}

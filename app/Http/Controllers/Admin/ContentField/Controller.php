<?php

namespace Factotum\Http\Controllers\Admin\ContentField;

use Factotum\Http\Controllers\Admin\Controller as MainAdminController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use Validator;

use Factotum\ContentType;
use Factotum\ContentField;
use Factotum\Library\Utility;


class Controller extends MainAdminController
{
	protected $fieldTypes = array(
		'text'                    => 'Text',
		'textarea'                => 'Textarea',
		'wysiwyg'                 => 'Wysiwyg Editor',
		'select'                  => 'Select',
		'multiselect'             => 'Multi Select',
		'checkbox'                => 'Checkbox',
		'multicheckbox'           => 'Multi Checkbox',
		'radio'                   => 'Radio',
		'date'                    => 'Date',
		'datetime'                => 'Date and Time',
		'file_upload'             => 'File Upload',
		'image_upload'            => 'Image Upload',
		'gallery'                 => 'Gallery',
		'linked_content'          => 'Linked Content',
		'multiple_linked_content' => 'Multiple Linked Content'
	);


	protected $imageOperations = array(
		'resize' => 'Resize',
		'crop'   => 'Crop',
		'fit'    => 'Fit'
	);

	protected $basicRules = [
		'label' => 'required',
		'name'  => 'required',
		'type'  => 'required',
	];

	protected $optionsRules = [
		'option_value' => 'required_if:type,select,multiselect,checkbox,multicheckbox,radio',
		'option_label' => 'required_if:type,select,multiselect,checkbox,multicheckbox,radio',
	];

	protected $fileRules = [
		'max_file_size' => 'required_if:type,file_upload,image_upload,gallery|numeric',
		'allowed_types' => 'required_if:type,file_upload,image_upload,gallery|allowed_types'
	];

	protected $imageRules = [
		'min_width_size'  => 'required_if:type,image_upload,gallery|numeric',
		'min_height_size' => 'required_if:type,image_upload,gallery|numeric',
		'image_operation' => 'required_if:type,image_upload,gallery',
		'width_resize'    => 'required_if:type,image_upload,gallery',
		'height_resize'   => 'required_if:type,image_upload,gallery'
	];


	protected $messages = [
		'allowed_types'  => 'The field :attribute is not in the right format.',
	];

	// TODO: lato frontend aggiungere controllo che non esista :/; all'interno delle options
	protected function _save( Request $request, $contentField )
	{
		$data = $request->all();

		$contentField->label     = $data['label'];
		$contentField->name      = $data['name'];
		$contentField->type      = $data['type'];
		$contentField->hint      = $data['hint'];
		$contentField->mandatory = (isset($data['mandatory']) ? true : false);

		if ($data['type'] == 'file_upload' || $data['type'] == 'image_upload' || $data['type'] == 'gallery') {
			$contentField->max_file_size = $data['max_file_size'];
			$contentField->allowed_types = $data['allowed_types'];
		} else {
			$contentField->max_file_size = null;
			$contentField->allowed_types = null;
		}

		if ($data['type'] == 'image_upload' || $data['type'] == 'gallery') {
			$contentField->min_width_size   = $data['min_width_size'];
			$contentField->min_height_size  = $data['min_height_size'];
			$contentField->image_operation  = $data['image_operation'];
			$contentField->resizes          = Utility::convertOptionsAssocArrayToString( $data['width_resize'], $data['height_resize'] );
		} else {
			$contentField->min_width_size   = null;
			$contentField->min_height_size  = null;
			$contentField->image_operation  = null;
			$contentField->resizes          = null;
		}

		if ($data['type'] == 'select' || $data['type'] == 'multiselect' ||
			$data['type'] == 'checkbox' || $data['type'] == 'multicheckbox' ||
			$data['type'] == 'radio' ) {
			$contentField->options = Utility::convertOptionsAssocArrayToString( $data['option_value'], $data['option_label'] );
		} else {
			$contentField->options = null;
		}

		if ($data['type'] == 'linked_content' || $data['type'] == 'multiple_linked_content') {
			$contentField->linked_content_type_id = $data['linked_content_type_id'];
		} else {
			$contentField->linked_content_type_id = null;
		}

		$contentField->save();
		return $contentField;
	}

	protected function validator(array $data, $id = null)
	{
		$rules = array_merge($this->basicRules, $this->optionsRules, $this->fileRules, $this->imageRules);

		if ($id) {
			$contentField = ContentField::where('name', $data['name'])
										->where('content_type_id', $data['content_type_id'])
										->first();
			if ($contentField && $contentField->id != $id ) {
				$rules['name'] .= '|unique:content_fields,name,NULL,id,content_type_id,' . $data['content_type_id'];
			}
		} else {
			$rules['name'] .= '|unique:content_fields,name,NULL,id,content_type_id,' . $data['content_type_id'];
		}

		$rules['name'] .= '|not_in:' . join(',', config('factotum.prohibited_content_field_names'));

		$validator = Validator::make($data, $rules);
		return $validator;
	}

}

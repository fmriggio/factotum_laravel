<?php

namespace Factotum\Http\Controllers\Admin\Capability;

use Factotum\Capability;
use Factotum\Role;
use Factotum\ContentType;

use Illuminate\Http\Request;

class UpdateController extends Controller
{
	public function edit($id)
	{
		$capability = Capability::find($id);

		$roles = Role::all();
		$contentTypes = ContentType::all();
		return view('admin.capability.edit')
			->with('title', 'Update')
			->with('postUrl', url('/admin/capability/update/' . $id) )
			->with('capability', $capability)
			->with('roles', $roles)
			->with('contentTypes', $contentTypes);
	}

	public function update(Request $request, $id)
	{
		$this->validator($request->all(), $id)->validate();

		$capability = Capability::find($id);
		$this->_save( $request, $capability );

		return redirect('admin/capability/list')->with('message', 'Successfully updated capability!');
	}
}

<?php

namespace Factotum\Http\Controllers\Admin\Capability;

use Factotum\Capability;

class DeleteController extends Controller
{
	public function delete($id)
	{
		Capability::destroy($id);
		return redirect('/admin/capability/list')->with('message', 'Capability deleted!');
	}
}

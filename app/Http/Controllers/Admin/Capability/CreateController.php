<?php

namespace Factotum\Http\Controllers\Admin\Capability;

use Factotum\Capability;
use Factotum\Role;
use Factotum\ContentType;

use Illuminate\Http\Request;

class CreateController extends Controller
{

	public function create()
	{
		$roles = Role::all();
		$contentTypes = ContentType::all();
		return view('admin.capability.edit')
			->with('title', 'Create')
			->with('postUrl', url('/admin/capability/store') )
			->with('roles', $roles)
			->with('contentTypes', $contentTypes);
	}

	public function store(Request $request)
	{
		$this->validator($request->all())->validate();

		$capability = new Capability;
		$this->_save( $request, $capability );

		return redirect('admin/capability/list')->with('message', 'Successfully created capability!');
	}

}


<?php

namespace Factotum\Http\Controllers\Admin;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected $validationMessages = [
		'required'    => 'Il campo :attribute è obbligatorio.',
		'email'       => 'Il campo :attribute non è una email valida',
		'min'         => 'Il campo :attribute deve essere lungo almeno :min caratteri.',
		'max'         => 'Il campo :attribute deve essere lungo al massimo :max caratteri.',
		'exists'      => 'Non esiste nessun utente con questa :attribute.',
		'date_format' => 'Il campo :attribute non è formattato correttamente'
	];

	protected function guard()
	{
		return Auth::guard();
	}

}

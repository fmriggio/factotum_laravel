<?php

namespace Factotum\Http\Controllers\Admin\Role;

use Factotum\Role;

use Illuminate\Http\Request;
use Validator;

class UpdateController extends Controller
{
	public function edit($id)
	{
		$role = Role::find($id);

		return view('admin.role.edit')
			->with('role', $role)
			->with('title', 'Update')
			->with('postUrl', url('/admin/role/update/' . $id ) );
	}

	public function update(Request $request, $id)
	{
		$this->validator($request->all(), $id)->validate();

		$role = Role::find($id);
		$this->_save( $request, $role );

		return redirect('admin/role/list')->with('message', 'Successfully updated role!');
	}
}

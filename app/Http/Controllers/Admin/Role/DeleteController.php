<?php

namespace Factotum\Http\Controllers\Admin\Role;

use Factotum\Role;
use Factotum\User;

class DeleteController extends Controller
{
	// TODO
	public function delete($id)
	{
		User::where('role_id', $id)->delete();
		Role::destroy($id);
		return redirect('/admin/role/list')->with('message', 'Role deleted!');
	}
}

<?php

namespace Factotum\Http\Controllers\Admin\Role;

use Factotum\Role;
use Illuminate\Http\Request;


class CreateController extends Controller
{

	public function create()
	{
		$roles = Role::all();
		return view('admin.role.edit')
			->with('title', 'Create')
			->with('postUrl', url('/admin/role/store') )
			->with('roles', $roles);
	}

	public function store(Request $request)
	{
		$this->validator($request->all())->validate();

		$role = new Role;
		$role = $this->_save( $request, $role );

		return redirect('admin/role/list')->with('message', 'Successfully created role!');
	}

}

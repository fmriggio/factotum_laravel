<?php

use Factotum\Library\Utility;

class PrintDropdownTree {

	protected static $current;
	protected static $name;
	protected static $multiple;

	public static function print_dropdown( $options, $name, $current = '', $id = '', $class = '', $multiple = false, $required = false )
	{
		self::$current  = $current;
		self::$name     = $name;
		self::$multiple = $multiple;
?>
		<select name="<?php echo $name . ($multiple ? '[]' : ''); ?>"
				id="<?php echo $id; ?>"
				class="<?php echo $class; ?>" autofocus
				<?php echo ( $multiple ? ' multiple': '' ) . ( $required ? ' required' : '' ); ?>>
			<?php if ( !$multiple ) { ?>
				<option value="">Seleziona la voce padre</option>
			<?php } ?>
			<?php self::print_options($options); ?>
		</select>
<?php
	}

	private static function print_options( $options, $counter = 0)
	{
		foreach( $options as $opt ) {
			if ( self::$multiple ) {
				if ( (is_array(old(self::$name)) && in_array($opt['id'], old(self::$name))) ||
					 (self::$current == $opt['id']) ||
					 (is_array(self::$current) && in_array($opt['id'], self::$current)) ) {
					$selected = ' selected';
				} else {
					$selected = '';
				}
			} else {
				$selected = ( self::$current == $opt['id'] || old(self::$name) == $opt['id'] ? ' selected' : '');
			}
?>
			<option value="<?php echo $opt['id']; ?>"<?php echo $selected; ?>><?php echo str_repeat('— ', $counter) . $opt['title']; ?></option>
<?php
			if ( count( $opt['childs'] ) > 0 ) {
				$counter++;
				self::print_options( $opt['childs'], $counter );
			}
		}
	}
}
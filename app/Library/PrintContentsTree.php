<?php

use Factotum\Library\Utility;

class PrintContentsTree {

	public static function print_list( $list )
	{
		self::print_items( $list );
	}

	private static function print_items( $list, $counter = 0)
	{
		foreach( $list as $content ) {

?>
			<tr>
				<th scope="row"><?php echo $content['id']; ?></th>
				<td>
					<a href="<?php echo url('/admin/content/edit/' . $content->id); ?>"><?php echo str_repeat('— ', $counter) . $content['title']; ?></a>
				</td>
				<td>
					<a href="<?php echo url('/admin/content/edit/' . $content->id); ?>">Edit</a>
				</td>
				<td>
					<a href="<?php echo url('/admin/content/delete/' . $content->id); ?>">Delete</a>
				</td>
			</tr>
<?php
			if ( count( $content['childs'] ) > 0 ) {
				$counter++;
				self::print_items( $content['childs'], $counter );
			}
		}
	}
}
<?php

use Factotum\Library\Utility;

class PrintField {

	protected static $field;
	protected static $errors;
	protected static $default;

	public static function print_field( $field, $errors, $default = null )
	{
		self::$field  = $field;
		self::$errors = $errors;

		if ( $default ) {
			self::$default = $default;
		} else {
			self::$default = null;
		}


?>
		<div class="form-group<?php echo ($errors->has('status') ? ' has-error' : '' ); ?>">
			<?php self::print_label(); ?>
			 <div class="col-md-10">
<?php
				switch ( self::$field->type ) {
					case 'text':
						self::print_input();
					break;
					case 'textarea':
						self::print_textarea();
					break;
					case 'wysiwyg':
						self::print_textarea( true );
					break;
					case 'date':
						self::print_date();
					break;
					case 'datetime':
						self::print_datetime();
					break;
					case 'checkbox':
						self::print_checkbox();
					break;
					case 'multicheckbox':
						self::print_multicheckbox();
					break;
					case 'select':
						self::print_select();
					break;
					case 'multiselect':
						self::print_multiselect();
					break;
					case 'radio':
						self::print_radio();
					break;
					case 'file_upload':
						self::print_file_upload();
					break;
					case 'image_upload':
						self::print_image_upload();
					break;
					case 'gallery':
						self::print_gallery();
					break;
					case 'linked_content':
						self::print_linked_content();
					break;
					case 'multiple_linked_content':
						self::print_multiple_linked_content();
					break;
				}
?>
			 </div>
		</div>

<?php
	}



	private static function print_label()
	{
?>
		<label for="<?php self::$field->name; ?>"
			   class="col-md-2 control-label">
			<?php echo self::$field->label . (self::$field->mandatory ? ' *' : ''); ?>
		</label>
<?php
	}



	private static function print_input()
	{
?>
		<input id="<?php echo self::$field->name; ?>" type="text" class="form-control"
			   name="<?php echo self::$field->name; ?>"
			   value="<?php echo old( self::$field->name, (isset(self::$default) ? self::$default : null)); ?>"
			   <?php echo ( self::$field->mandatory ? 'required' : '' ); ?> autofocus>

<?php
	}


	private static function print_textarea( $wysiwyg = false)
	{
?>
		<textarea class="form-control<?php echo ($wysiwyg ? ' wysiwyg' : ''); ?>"
			id="<?php echo self::$field->name; ?>"
			name="<?php echo self::$field->name; ?>"
			<?php echo ( self::$field->mandatory ? 'required' : '' ) . (!$wysiwyg ? ' autofocus' : ''); ?>><?php echo old( self::$field->name, (isset(self::$default) ? self::$default : null)); ?></textarea>

<?php
	}


	private static function print_date()
	{
		if ( self::$default ) {
			self::$default = Utility::convertIsoDateToHuman( self::$default );
		}
?>
		<input id="<?php echo self::$field->name; ?>" type="text" class="form-control date"
			   name="<?php echo self::$field->name; ?>"
			   value="<?php echo old( self::$field->name, (isset(self::$default) ? self::$default : null)); ?>"
			<?php echo ( self::$field->mandatory ? 'required' : '' ); ?> autofocus>
<?php
	}


	private static function print_datetime()
	{
		if ( self::$default ) {
			self::$default = Utility::convertIsoDateTimeToHuman( self::$default );
		}
?>
		<input id="<?php echo self::$field->name; ?>" type="text" class="form-control datetime"
			   name="<?php echo self::$field->name; ?>"
			   value="<?php echo old( self::$field->name, (isset(self::$default) ? self::$default : null)); ?>"
			<?php echo ( self::$field->mandatory ? 'required' : '' ); ?> autofocus>
		<?php
	}


	private static function print_checkbox()
	{
		$options = Utility::convertOptionsTextToAssocArray(self::$field->options);
?>
		<div class="form-control" style="border: none; box-shadow: none;">
			<input type="checkbox" id="<?php echo self::$field->name; ?>"
				   name="<?php echo self::$field->name; ?>"
				   value="<?php echo key($options); ?>"
				   <?php echo ( self::$field->mandatory ? 'required' : '' ); ?>
				   <?php echo ( (isset(self::$default) && self::$default == key($options)) || ( old(self::$field->name) == key($options) ) ? ' checked' : '' ); ?>>
			<label for="<?php echo self::$field->name; ?>"><?php echo array_pop($options); ?></label>
		</div>
<?php
	}


	// TODO: add custom frontend check for multicheckbox
	private static function print_multicheckbox()
	{
		$options = Utility::convertOptionsTextToAssocArray(self::$field->options);
		if ( self::$default ) {
			self::$default = Utility::convertOptionsTextToArray(self::$default);
		}
?>
		<div class="form-control" style="border: none; box-shadow: none;">
<?php
			if ( count($options) > 0 ) {

				$index = 0;
				foreach ( $options as $value => $label ) {
					$check = false;
					if ( (isset(self::$default) && in_array($value, self::$default)) ||
						 ( is_array(old(self::$field->name)) && in_array($value, old(self::$field->name)) ) ) {
						$check = true;
					}
?>
					<input type="checkbox" id="<?php echo self::$field->name . '_' . $index; ?>"
						   name="<?php echo self::$field->name; ?>[]"
						   <?php echo ( $check ? 'checked="checked"' : null ); ?>
						   <?php //echo ( self::$field->mandatory ? 'required' : '' ); ?>
						   value="<?php echo $value; ?>" />
					<label for="<?php echo self::$field->name . '_' . $index; ?>"><?php echo $label; ?></label>
<?php
					$index++;
				}
			}
?>
		</div>
<?php
	}



	private static function print_select()
	{
		$options = Utility::convertOptionsTextToAssocArray( self::$field->options );
?>
		<select name="<?php echo self::$field->name; ?>"
				id="<?php echo self::$field->name; ?>"
				class="form-control" autofocus
				<?php echo ( self::$field->mandatory ? 'required' : '' ); ?>>
<?php
				if ( count($options) > 0 ) {

					foreach ($options as $value => $label) {

						$check = false;
						if ( (isset(self::$default) && self::$default == $value) ||
							( old(self::$field->name) == $value )) {
							$check = true;
						}
?>
						<option value="<?php echo $value; ?>"
							<?php echo ( $check ? 'selected="selected"' : null ); ?>>
							<?php echo $label; ?>
						</option>
<?php
					}
				}
?>
		</select>
<?php

	}



	private static function print_multiselect()
	{
		$options = Utility::convertOptionsTextToAssocArray( self::$field->options );

		if ( self::$default ) {
			self::$default = Utility::convertOptionsTextToArray(self::$default);
		}
?>
		<select name="<?php echo self::$field->name; ?>[]" multiple
				id="<?php echo self::$field->name; ?>"
				class="form-control" autofocus
			<?php echo ( self::$field->mandatory ? 'required' : '' ); ?>>
			<?php
			if ( count($options) > 0 ) {

				foreach ($options as $value => $label) {

					$check = false;
					if ( ( isset(self::$default) && in_array($value, self::$default) ) ||
						 (  is_array(old(self::$field->name)) && in_array($value, old(self::$field->name)) ) ) {
						$check = true;
					}
?>
					<option value="<?php echo $value; ?>"
						<?php echo ( $check ? 'selected="selected"' : null ); ?>>
						<?php echo $label; ?>
					</option>
					<?php
				}
			}
			?>
		</select>
<?php
	}


	private static function print_radio()
	{
		$options = Utility::convertOptionsTextToAssocArray(self::$field->options);
?>
		<div class="form-control" style="border: none; box-shadow: none;">
<?php
			$index = 0;
			foreach ( $options as $value => $label ) { ?>
				<input type="radio" id="<?php echo self::$field->name . '_' . $index; ?>"
					   name="<?php echo self::$field->name; ?>"
					   value="<?php echo $value; ?>"
					<?php echo ( self::$field->mandatory ? 'required' : '' ); ?>
					<?php echo ( (isset(self::$default) && self::$default == $value) || ( old(self::$field->name) == $value)  ? ' checked' : '' ); ?>>
				<label for="<?php echo self::$field->name . '_' . $index; ?>"><?php echo $label; ?></label>
<?php
				$index++;
			}
?>

		</div>
<?php
	}


	private static function print_file_upload()
	{
		$required = '';
		if ( self::$field->mandatory ) {
			if ( !self::$default ) {
				$required = 'required';
			} else {
				$required = '';
			}
		}
?>

		<input id="<?php echo self::$field->name; ?>" type="file" class="form-control"
			   name="<?php echo self::$field->name; ?>"	<?php echo $required; ?> autofocus>

		<?php if ( self::$default ) { ?>
			<?php echo self::$default['url']; ?>
		<?php } ?>

		<?php if ( self::$default ) { ?>
		<input type="hidden" id="<?php echo self::$field->name; ?>_hidden"
			   name="<?php echo self::$field->name; ?>_hidden"
			   value="<?php echo self::$default['id']; ?>">
		<?php } ?>

<?php
	}


	private static function print_image_upload()
	{
		$required = '';
		if ( self::$field->mandatory ) {
			if ( !self::$default ) {
				$required = 'required';
			} else {
				$required = '';
			}
		}

?>

		<input id="<?php echo self::$field->name; ?>" type="file" class="form-control" accept="image/*"
			   name="<?php echo self::$field->name; ?>" <?php echo $required; ?> autofocus>

		<?php if ( self::$default ) { ?>
			<img src="<?php echo asset( self::$default['url'] ); ?>" width="50%" />
		<?php } ?>

		<?php if ( self::$default ) { ?>
		<input type="hidden" id="<?php echo self::$field->name; ?>_hidden"
			   name="<?php echo self::$field->name; ?>_hidden"
			   value="<?php echo self::$default['id']; ?>">
		<?php } ?>

<?php

	}


	private static function print_gallery()
	{
		$required = '';
		if ( self::$field->mandatory ) {
			if ( !self::$default ) {
				$required = 'required';
			} else {
				$required = '';
			}
		}

?>

		<input id="<?php echo self::$field->name; ?>" type="file" class="form-control" accept="image/*" multiple
			   name="<?php echo self::$field->name; ?>[]" <?php echo $required; ?> autofocus>

<?php
		if ( isset(self::$default) && count(self::$default) > 0 ) {
			$tmp = array();
			foreach ( self::$default as $image ) {
				$tmp[] = $image['id'];
?>
				<img src="<?php echo asset( $image['url'] ); ?>" width="30%" />
<?php
			}
?>
			<input type="hidden" id="<?php echo self::$field->name; ?>_hidden"
			   name="<?php echo self::$field->name; ?>_hidden"
			   value="<?php echo join(';', $tmp); ?>">
<?php
		}

	}

	private static function print_linked_content()
	{
		PrintDropdownTree::print_dropdown(self::$field->options, self::$field->name, self::$default, self::$field->name, 'form-control', false, self::$field->mandatory);
	}

	private static function print_multiple_linked_content()
	{
		self::$default = Utility::convertOptionsTextToArray( self::$default );

		PrintDropdownTree::print_dropdown(self::$field->options, self::$field->name, self::$default, self::$field->name, 'form-control', true, self::$field->mandatory);
	}










	private static function print_errors()
	{
		if (self::$errors->has( self::$field->name )) {
?>
			<span class="help-block">
				<strong><?php echo self::$errors->first( self::$field->name ); ?></strong>
            </span>
<?php
		}
	}

}





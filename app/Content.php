<?php

namespace Factotum;

use Illuminate\Database\Eloquent\Model;

use Factotum\Library\Utility;

class Content extends Model
{
	public function childs() {
		return $this->hasMany('Factotum\Content','parent_id','id') ;
	}

	public static function treeArray( $contentTypeId, $pagination = null)
	{
		$contents = self::_getContents( $contentTypeId, $pagination );

		if ($contents->count() > 0) {
			$contents = self::_parseTree($contents);
		}
		return $contents->toArray();
	}


	public static function treeObjects( $contentTypeId, $pagination = null)
	{
		$contents = self::_getContents( $contentTypeId, $pagination );

		if ($contents->count() > 0) {
			$contents = self::_parseTree($contents);
		}
		return $contents;
	}

	private static function _getContents( $contentTypeId, $pagination )
	{
		if ( $pagination ) {
			return Content::where( 'content_type_id', '=', $contentTypeId )
							->whereNull( 'parent_id' )
							->paginate($pagination);
		} else {
			return Content::where( 'content_type_id', '=', $contentTypeId )
							->whereNull( 'parent_id' )
							->get();
		}
	}

	private static function _parseTree($contents)
	{
		foreach ($contents as $c) {
			if ( count($c->childs) > 0 ) {
				$c->childs = self::_parseTree($c->childs);
			}
		}
		return $contents;
	}
}

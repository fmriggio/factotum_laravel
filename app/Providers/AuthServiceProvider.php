<?php

namespace Factotum\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Factotum\Model' => 'Factotum\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        $this->registerPolicies();

//        echo '<pre>';
//		print_r($request->path());

/*
		Gate::define('update-post', function ($user, $post) {
			return $user->id == $post->user_id;
		});*/
    }

}

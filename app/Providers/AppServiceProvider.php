<?php

namespace Factotum\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Validator;

use Factotum\ContentType;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		Validator::extend('allowed_types', function($attribute, $value, $parameters, $validator) {
			if ($value == '*') {
				return true;
			}
			$formats = explode(',', $value);
			if (count($formats) > 0) {
				$error = false;
				foreach ($formats as $format) {
					if (strlen($format) < 3) {
						$error = true;
					}
				}
				return !$error;
			}
			return $value == 'foo';
		});

		$contentTypes = ContentType::all();
		View::share('contentTypes', $contentTypes);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}

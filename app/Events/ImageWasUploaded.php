<?php

namespace Factotum\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ImageWasUploaded
{
	private $path;

	public function __construct($path)
	{
		$this->path = $path;
	}

	/**
	 * @return string
	 */
	public function path()
	{
		return $this->path;
	}

}

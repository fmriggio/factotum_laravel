<?php

namespace Factotum;

use Factotum\Notifications\AdminResetPassword as AdminResetPasswordNotification;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function profile() {
		return $this->hasOne('Factotum\Profile');
	}

	public function role() {
		return $this->hasOne('Factotum\Role');
	}

	/**
	 * Send the password reset notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendPasswordResetNotification($token)
	{
		// TODO: check user role, if he had access to the backend, select different notification
		// based on the role.
		$this->notify(new AdminResetPasswordNotification($token));
	}

}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
	return redirect('/admin/user/list');
});

// Factotum - Auth Routes / User Route - Guest
Route::group([  'prefix' => 'admin',
				'namespace' => 'Admin',
				'middleware' => 'guest' ], function() {

	Route::get('/auth/login', 'Auth\LoginController@index');
	Route::post('/auth/login', 'Auth\LoginController@login');

	Route::get('/user/register', 'User\RegisterController@index');
	Route::post('/user/register', 'User\RegisterController@register');

	Route::get('/auth/password/reset', 'Auth\ForgotPasswordController@index');
	Route::post('/auth/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
	Route::get('/auth/password/reset/{token}', 'Auth\ForgotPasswordController@reset');
	Route::post('/auth/password/reset', 'Auth\ForgotPasswordController@resetUserPassword');

});

// Factotum - Auth Routes - Logged In
Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin',
	'middleware' => 'auth' ], function() {
	Route::get('/auth/logout', 'Auth\LogoutController@logout');
});


Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin',
	'middleware' => 'auth' ], function() {


	// Factotum - User Routes - Logged In
	Route::get('/user/list', 'User\ReadController@index');
	Route::get('/user/detail/{id}', 'User\ReadController@detail');
	Route::get('/user/create', 'User\CreateController@create');
	Route::post('/user/store', 'User\CreateController@store');
	Route::get('/user/edit/{id}', 'User\UpdateController@edit');
	Route::post('/user/update/{id}', 'User\UpdateController@update');
	Route::get('/user/delete/{id}', 'User\DeleteController@delete');

	// Factotum - Role Routes - Logged In
	Route::get('/role/list', 'Role\ReadController@index');
	Route::get('/role/detail/{id}', 'Role\ReadController@detail');
	Route::get('/role/create', 'Role\CreateController@create');
	Route::post('/role/store', 'Role\CreateController@store');
	Route::get('/role/edit/{id}', 'Role\UpdateController@edit');
	Route::post('/role/update/{id}', 'Role\UpdateController@update');
	Route::get('/role/delete/{id}', 'Role\DeleteController@delete');

	// Factotum - Content Type Routes - Logged In
	Route::get('/content-type', function() { return redirect('/admin/content-type/list'); });
	Route::get('/content-type/list', 'ContentType\ReadController@index');
	Route::get('/content-type/detail/{id}', 'ContentType\ReadController@detail');
	Route::get('/content-type/create', 'ContentType\CreateController@create');
	Route::post('/content-type/store', 'ContentType\CreateController@store');
	Route::get('/content-type/edit/{id}', 'ContentType\UpdateController@edit');
	Route::post('/content-type/update/{id}', 'ContentType\UpdateController@update');
	Route::get('/content-type/delete/{id}', 'ContentType\DeleteController@delete');


	// Factotum - Capability Routes - Logged In
	Route::get('/capability/list', 'Capability\ReadController@index');
	Route::get('/capability/detail/{id}', 'Capability\ReadController@detail');
	Route::get('/capability/create', 'Capability\CreateController@create');
	Route::post('/capability/store', 'Capability\CreateController@store');
	Route::get('/capability/edit/{id}', 'Capability\UpdateController@edit');
	Route::post('/capability/update/{id}', 'Capability\UpdateController@update');
	Route::get('/capability/delete/{id}', 'Capability\DeleteController@delete');

	// Factotum - Content Fields Routes - Logged In
	Route::get('/content-field', function() { return redirect('/admin/content-field/list'); });
	Route::get('/content-field/list', 'ContentField\ReadController@index');
	Route::get('/content-field/detail/{id}', 'ContentField\ReadController@detail');
	Route::get('/content-field/create/{content_type_id}', 'ContentField\CreateController@create');
	Route::post('/content-field/store/{content_type_id}', 'ContentField\CreateController@store');
	Route::get('/content-field/edit/{content_type_id}/{id}', 'ContentField\UpdateController@edit');
	Route::post('/content-field/update/{content_type_id}/{id}', 'ContentField\UpdateController@update');
	Route::get('/content-field/delete/{id}', 'ContentField\DeleteController@delete');


	// Factotum - Content Routes - Logged In
	Route::get('/content/list/{content_type_id}', 'Content\ReadController@indexList');
	Route::get('/content/detail/{id}', 'Content\ReadController@detail');
	Route::get('/content/create/{content_type_id}', 'Content\CreateController@create');
	Route::post('/content/store/{content_type_id}', 'Content\CreateController@store');
	Route::get('/content/edit/{id}', 'Content\UpdateController@edit');
	Route::post('/content/update/{id}', 'Content\UpdateController@update');
	//Route::get('/content-field/delete/{id}', 'ContentField\DeleteController@delete');






//	// Factotum - Media Routes - Logged In
//	Route::get('/media', 'Media\ReadController@show');
//	Route::any('/media/upload', 'Media\UploadController@upload');
//	Route::get('/media/json-items', 'Media\ReadController@getItems');
//	// folders
//	Route::get('/media/add-folder', 'Media\FolderController@addFolder');
//	Route::get('/media/deletefolder', 'Media\FolderController@getDeletefolder');
//	Route::get('/media/folders', 'Media\FolderController@getFolders');
//	// crop
//	Route::get('/media/crop', 'Media\CropController@getCrop');
//	Route::get('/media/cropimage', 'Media\CropController@getCropimage');
//	// rename
//	Route::get('/media/rename', 'Media\RenameController@getRename');
//	// scale/resize
//	Route::get('/media/resize', 'Media\ResizeController@getResize');
//	Route::get('/media/doresize', 'Media\ResizeController@performResize');
//	// download
//	Route::get('/media/download', 'Media\DownloadController@getDownload');
//	// delete
//	Route::get('/media/delete', 'Media\DeleteController@getDelete');


});


Route::get('/home', 'HomeController@index');

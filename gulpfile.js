const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

	mix.scripts([
		// jQuery + Bootstrap
		'./bower_components/jquery/dist/jquery.min.js',
		'./bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',

		// Froala
		'./bower_components/froala-wysiwyg-editor/js/froala_editor.min.js',
		'./node_modules/codemirror/lib/codemirror.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/align.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/char_counter.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/code_beautifier.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/code_view.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/colors.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/emoticons.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/entities.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/file.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/font_family.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/font_size.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/fullscreen.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/image.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/image_manager.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/inline_style.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/line_breaker.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/link.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/lists.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/paragraph_format.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/paragraph_style.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/quick_insert.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/quote.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/table.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/save.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/url.min.js',
		'./bower_components/froala-wysiwyg-editor/js/plugins/video.min.js',
		'./bower_components/froala-wysiwyg-editor/js/languages/it.js',

		// jQuery Plugins
		'./bower_components/jquery-ui/jquery-ui.min.js',
		'./bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js',


		// Content Field
		'factotum/content-field/main.js',
		'factotum/content-field/options.js',
		'factotum/content-field/files.js',
		'factotum/content-field/images.js',
		'factotum/content-field/linked-content.js',

		// Content
		'factotum/content/main.js',

		// Utilities
		'factotum/utility.js'

	], 'public/factotum/js/main.js');

	mix.copy( './bower_components/font-awesome/fonts/**', 'public/factotum/fonts/');
	mix.copy( './bower_components/jquery-ui/themes/base/images/**', 'public/factotum/css/images/');

	mix.sass([
		'./bower_components/font-awesome/scss/font-awesome.scss',
		'factotum/main.scss'
	], 'resources/assets/css/factotum/main.css')
	.styles([

		// Froala
		'./bower_components/froala-wysiwyg-editor/css/froala_editor.css',
		'./bower_components/froala-wysiwyg-editor/css/froala_style.css',
		'./node_modules/codemirror/lib/codemirror.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/char_counter.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/code_view.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/colors.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/emoticons.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/file.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/fullscreen.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/image.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/image_manager.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/line_breaker.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/quick_insert.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/table.css',
		'./bower_components/froala-wysiwyg-editor/css/plugins/video.css',

		// Bootstrap and Plugins
		'./bower_components/jquery-ui/themes/base/theme.css',
		'./bower_components/jquery-ui/themes/base/datepicker.css',
		'./bower_components/datetimepicker/build/jquery.datetimepicker.min.css',

		'resources/assets/css/factotum/main.css'
	], 'public/factotum/css/main.css');




	// Website Frontend Stuff
	mix.sass('app.scss');

	mix.scripts([
		'app.js'
	], 'public/js/app.js');


});
